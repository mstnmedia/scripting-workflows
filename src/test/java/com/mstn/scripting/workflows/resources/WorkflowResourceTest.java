///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.workflows.resources;
//
//import com.mstn.scripting.core.Representation;
//import com.mstn.scripting.core.db.WhereClause;
//import com.mstn.scripting.core.models.Workflow;
//
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.eq;
//import static org.mockito.Mockito.any;
//import static org.mockito.Mockito.reset;
//import static org.mockito.Mockito.verify;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertNotNull;
//
//import com.mstn.scripting.workflows.core.WorkflowService;
//import com.mstn.scripting.workflows.core.WorkflowStepService;
//import io.dropwizard.testing.junit.ResourceTestRule;
//import java.util.ArrayList;
//import java.util.List;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.core.MediaType;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.ClassRule;
//import org.junit.Test;
//import org.mockito.ArgumentMatchers;
//
///**
// *
// * @author josesuero
// */
//public class WorkflowResourceTest {
//
//	private static final String SUCCESS = "Success...";
//	private static final String TEST_WORKFLOW_NAME = "testWorkflowName";
//	private static final String TEST_WORKFLOW_TAGS = "testWorkflowTags";
//	private static final String WORKFLOW_ENDPOINT = "/workflow";
//
//	private static final WorkflowService WORKFLOWSERVICE = mock(WorkflowService.class);
//	private static final WorkflowStepService WORKFLOWSTEPSERVICE = mock(WorkflowStepService.class);
//
//	@ClassRule
//	public static final ResourceTestRule RESOURCES = ResourceTestRule.builder().addResource(new WorkflowResource(WORKFLOWSERVICE, WORKFLOWSTEPSERVICE)).build();
//
//	private static final Workflow WORKFLOW = new Workflow(0, 0, TEST_WORKFLOW_NAME, TEST_WORKFLOW_TAGS, 0);
//	//private static final Workflow WORKFLOW_STRUCTURE = new Workflow(0, TEST_WORKFLOW_NAME, TEST_WORKFLOW_TAGS, 0);
//
//	@Before
//	public void setup() {
//		//when(workflowService.getWorkflow(eq(1))).thenReturn(workflow);
//
//		List<Workflow> workflows = new ArrayList<>();
//		workflows.add(WORKFLOW);
//
//		when(WORKFLOWSERVICE.getAll(any(WhereClause.class))).thenReturn(workflows);
//		when(WORKFLOWSERVICE.get(eq(1), any(WorkflowStepService.class))).thenReturn(WORKFLOW);
//		when(WORKFLOWSERVICE.create(any(Workflow.class), any(WorkflowStepService.class))).thenReturn(WORKFLOW);
//		when(WORKFLOWSERVICE.update(any(Workflow.class), any(WorkflowStepService.class))).thenReturn(WORKFLOW);
//		when(WORKFLOWSERVICE.delete(eq(1))).thenReturn(SUCCESS);
//	}
//
//	@After
//	public void tearDown() {
//		reset(WORKFLOWSERVICE);
//	}
//
//	@Test
//	public void testGetWorkflows() {
//		List<Workflow> workflows = RESOURCES.target(WORKFLOW_ENDPOINT)
//				.queryParam("where", "[]")
//				.request()
//				.get(TestWorkflowsRepresentation.class)
//				.getItems();
//		assertThat(workflows.size()).isEqualTo(1);
//		assertThat(workflows.get(0).getId()).isEqualTo(WORKFLOW.getId());
//		assertThat(workflows.get(0).getId_center()).isEqualTo(WORKFLOW.getId_center());
//		assertThat(workflows.get(0).getName()).isEqualTo(WORKFLOW.getName());
//		assertThat(workflows.get(0).getTags()).isEqualTo(WORKFLOW.getTags());
//
//		verify(WORKFLOWSERVICE).getAll(ArgumentMatchers.refEq(new WhereClause("[]"), ""));
//	}
//
//	@Test
//	public void testGetWorkflow() {
//		List<Workflow> workflowResponse = RESOURCES.target(WORKFLOW_ENDPOINT + "/1")
//				.request()
//				.get(TestWorkflowRepresentation.class)
//				.getItems();
//		assertThat(workflowResponse.get(0).getId()).isEqualTo(WORKFLOW.getId());
//		assertThat(workflowResponse.get(0).getId_center()).isEqualTo(WORKFLOW.getId_center());
//		assertThat(workflowResponse.get(0).getName()).isEqualTo(WORKFLOW.getName());
//		assertThat(workflowResponse.get(0).getTags()).isEqualTo(WORKFLOW.getTags());
//		assertThat(workflowResponse.get(0).getMain()).isEqualTo(WORKFLOW.getMain());
//		verify(WORKFLOWSERVICE).get(1, WORKFLOWSTEPSERVICE);
//	}
//
//	@Test
//	public void testCreateWorkflow() {
//		List<Workflow> newWorkflow;
//		newWorkflow = RESOURCES.target(WORKFLOW_ENDPOINT).request()
//				.post(Entity.entity(WORKFLOW, MediaType.APPLICATION_JSON_TYPE), TestWorkflowRepresentation.class)
//				.getItems();
//		assertNotNull(newWorkflow);
//		assertThat(newWorkflow.get(0).getId()).isEqualTo(WORKFLOW.getId());
//		assertThat(newWorkflow.get(0).getId_center()).isEqualTo(WORKFLOW.getId_center());
//		assertThat(newWorkflow.get(0).getName()).isEqualTo(WORKFLOW.getName());
//		assertThat(newWorkflow.get(0).getTags()).isEqualTo(WORKFLOW.getTags());
//		assertThat(newWorkflow.get(0).getMain()).isEqualTo(WORKFLOW.getMain());
//		verify(WORKFLOWSERVICE).create(any(Workflow.class), any(WorkflowStepService.class));
//	}
//
//	@Test
//	public void testEditWorkflow() {
//		List<Workflow> editedWorkflow = RESOURCES.target(WORKFLOW_ENDPOINT + "/1").request()
//				.put(Entity.entity(WORKFLOW, MediaType.APPLICATION_JSON_TYPE), TestWorkflowRepresentation.class)
//				.getItems();
//		assertNotNull(editedWorkflow);
//		assertThat(editedWorkflow.get(0).getId()).isEqualTo(WORKFLOW.getId());
//		assertThat(editedWorkflow.get(0).getId_center()).isEqualTo(WORKFLOW.getId_center());
//		assertThat(editedWorkflow.get(0).getName()).isEqualTo(WORKFLOW.getName());
//		assertThat(editedWorkflow.get(0).getTags()).isEqualTo(WORKFLOW.getTags());
//		assertThat(editedWorkflow.get(0).getMain()).isEqualTo(WORKFLOW.getMain());
//		verify(WORKFLOWSERVICE).update(any(Workflow.class), any(WorkflowStepService.class));
//	}
//
//	@Test
//	public void testDeleteWorkflow() {
//		assertThat(RESOURCES.target(WORKFLOW_ENDPOINT + "/1").request()
//				.delete(TestDeleteRepresentation.class).getItems()).isEqualTo(SUCCESS);
//		verify(WORKFLOWSERVICE).delete(1);
//	}
//
//	private static class TestWorkflowRepresentation extends Representation<List<Workflow>> {
//
//	}
//
//	private static class TestWorkflowsRepresentation extends Representation<List<Workflow>> {
//
//	}
//
//	private static class TestDeleteRepresentation extends Representation<String> {
//
//	}
//
//}
