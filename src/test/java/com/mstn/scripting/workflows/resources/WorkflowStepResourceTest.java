///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.workflows.resources;
//
//import com.mstn.scripting.core.db.WhereClause;
//import com.mstn.scripting.workflows.core.WorkflowStepService;
//import com.mstn.scripting.core.models.Workflow_Step;
//import io.dropwizard.testing.junit.ResourceTestRule;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import org.junit.Before;
//import org.junit.ClassRule;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
///**
// *
// * @author josesuero
// */
//public class WorkflowStepResourceTest {
//    private static final String SUCCESS = "Success...";
//    private static final String TEST_NAME = "testWorkflowName";
//    private static final String WORKFLOW_ENDPOINT = "/workflowstep";
//
//    private static final WorkflowStepService WORKFLOWSTEPSERVICE = mock(WorkflowStepService.class);
//
//    @ClassRule
//    public static final ResourceTestRule RESOURCES = ResourceTestRule.builder().addResource(new WorkflowStepResource(WORKFLOWSTEPSERVICE)).build();
//
//    private static final Workflow_Step WORKFLOW_STEP = new Workflow_Step(0, 0, 0, 0, "", 0, TEST_NAME, 0, 0);
//
//    @Before
//    public void setup() throws IOException {
//        //when(workflowService.getWorkflow(eq(1))).thenReturn(workflow);
//
//        List<Workflow_Step> items = new ArrayList<>();
//        items.add(WORKFLOW_STEP);
//        
//        when(WORKFLOWSTEPSERVICE.getAll(any(WhereClause.class))).thenReturn(items);
//        when(WORKFLOWSTEPSERVICE.get(eq(1))).thenReturn(WORKFLOW_STEP);
//        when(WORKFLOWSTEPSERVICE.create(any(Workflow_Step.class))).thenReturn(WORKFLOW_STEP);
//        when(WORKFLOWSTEPSERVICE.update(any(Workflow_Step.class))).thenReturn(WORKFLOW_STEP);
//        when(WORKFLOWSTEPSERVICE.delete(eq(1))).thenReturn(SUCCESS);
//    }
//
//}
