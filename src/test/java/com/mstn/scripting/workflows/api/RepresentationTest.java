///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.workflows.api;
//
//import com.mstn.scripting.core.models.Workflow;
//import static io.dropwizard.testing.FixtureHelpers.fixture;
//import static org.assertj.core.api.Assertions.assertThat;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import io.dropwizard.jackson.Jackson;
//import org.junit.Test;
//
///**
// *
// * @author josesuero
// */
//public class RepresentationTest {
//
//    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
//    private static final String WORKFLOW_JSON = "fixtures/workflow.json";
//    private static final String TEST_WORKFLOW_NAME = "testWorkflowName";
//    private static final String TEST_WORKFLOW_TAGS = "testWorkflowTags";
//    
//    @Test
//    public void serializesToJSON() throws Exception {
//        final Workflow workflow = new Workflow(0, 0, TEST_WORKFLOW_NAME, TEST_WORKFLOW_TAGS, 0);
//        
//        final String expected = MAPPER.writeValueAsString(MAPPER.readValue(fixture(WORKFLOW_JSON), Workflow.class));
//        
//        assertThat(MAPPER.writeValueAsString(workflow)).isEqualTo(expected);
//    }
//    
//    @Test
//    public void deserializesFromJSON() throws Exception {
//       final Workflow workflow = new Workflow(0, 0, TEST_WORKFLOW_NAME, TEST_WORKFLOW_TAGS, 0);
//
//       assertThat(MAPPER.readValue(fixture(WORKFLOW_JSON), Workflow.class).getId()).isEqualTo(workflow.getId());
//       assertThat(MAPPER.readValue(fixture(WORKFLOW_JSON), Workflow.class).getName())
//           .isEqualTo(workflow.getName());
//       assertThat(MAPPER.readValue(fixture(WORKFLOW_JSON), Workflow.class).getTags())
//           .isEqualTo(workflow.getTags());
//       assertThat(MAPPER.readValue(fixture(WORKFLOW_JSON), Workflow.class).getMain())
//           .isEqualTo(workflow.getMain());
//     }
//
//}
