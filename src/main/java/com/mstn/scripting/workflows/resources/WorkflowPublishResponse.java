package com.mstn.scripting.workflows.resources;

import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa los resultados del proceso de publicación de un flujo.
 *
 * @author MSTN Media
 */
public class WorkflowPublishResponse {

	/**
	 *
	 */
	public int id_workflow;

	/**
	 *
	 */
	public int id_version;

	/**
	 *
	 */
	public int result;

	/**
	 *
	 */
	public Workflow workflow;

	/**
	 *
	 */
	public WorkflowPublishParams config;

	/**
	 *
	 */
	public List<Workflow> subWorkflows = new ArrayList<>();

	/**
	 *
	 */
	public HashMap<Integer, List<Workflow_Step_Option>> mapSubWorkflowReturnedValues = new HashMap<>();

	/**
	 *
	 */
	public HashMap<Integer, Workflow> mapSubWorkflows = new HashMap<>();

	/**
	 *
	 */
	public HashMap<Integer, Workflow_Step> mapSteps = new HashMap<>();

	/**
	 *
	 */
	public List<Workflow_Step> steps = new ArrayList<>();

	/**
	 *
	 */
	public List<Workflow_Step> referers = new ArrayList<>();

	/**
	 *
	 */
	public List<IdValueParams> notes = new ArrayList<>();

	/**
	 *
	 */
	public WorkflowPublishResponse() {
	}

	/**
	 *
	 * @param code
	 * @param note
	 */
	public void addNote(int code, String note) {
		notes.add(new IdValueParams(code, note));
	}

}
