package com.mstn.scripting.workflows.resources;

/**
 * Clase que define configuración necesaria para publicar un flujo.
 *
 * @author MSTN Media
 */
public class WorkflowPublishParams {

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_version;

	/**
	 *
	 */
	protected String version;

	/**
	 *
	 */
	protected String notes;

	/**
	 *
	 */
	public WorkflowPublishParams() {
	}

	/**
	 *
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param version
	 * @param notes
	 */
	public WorkflowPublishParams(int id_workflow, int id_workflow_version, String version, String notes) {
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.version = version;
		this.notes = notes;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 *
	 * @param id_workflow_version
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 *
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	/**
	 *
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 *
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 *
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
