/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.workflows.core.WorkflowStepContentService;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a la relación
 * paso-contenido.
 *
 * @author josesuero
 */
@Path("/workflowStepContent")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.WORKFLOWS_LIST})
public class WorkflowStepContentResource {

	private final WorkflowStepContentService service;

	/**
	 *
	 * @param service
	 */
	public WorkflowStepContentResource(WorkflowStepContentService service) {
		this.service = service;
	}
	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Workflow_Step_Content>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) {
		try {
			WhereClause whereClause = service.getUserWhere(user, Where.listFrom(where));
			Representation<List<Workflow_Step_Content>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Workflow_Step_Content>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			WhereClause whereClause = service.getUserWhere(user, Where.listFrom(where));
			List<Workflow_Step_Content> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow_Step_Content>> get(@Auth User user, @PathParam("id") final int id) {
		Workflow_Step_Content item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.WORKFLOWS_ADD})
	public Representation<List<Workflow_Step_Content>> create(@Auth User user, @NotNull @Valid final Workflow_Step_Content item) {
		try {
			Workflow_Step_Content created = service.create(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_EDIT})
	public Representation<List<Workflow_Step_Content>> edit(@Auth User user,
			@NotNull @Valid final Workflow_Step_Content item, @PathParam("id") final int id) {
		try {
			item.setId(id);
			List<Workflow_Step_Content> list = new ArrayList<>();
			list.add(service.update(item, user));
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
