/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.workflows.core.ContentService;
import com.mstn.scripting.core.models.Content;
import com.mstn.scripting.core.models.IdValueParams;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a contenidos.
 *
 * @author amatos
 */
@Path("/content")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.CONTENTS_LIST})
public class ContentResource {

	private final ContentService service;

	/**
	 *
	 * @param contentService
	 */
	public ContentResource(ContentService contentService) {
		this.service = contentService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Content>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			Representation<List<Content>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Content>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Content> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Content>> get(@Auth User user, @PathParam("id") final int id) {
		Content content = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(content));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.CONTENTS_ADD})
	public Representation<List<Content>> create(@Auth User user, @NotNull @Valid final Content item) {
		try {
			Content inserted = service.create(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(inserted));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CONTENTS_EDIT})
	public Representation<List<Content>> edit(
			@Auth User user,
			@NotNull @Valid final Content item,
			@PathParam("id") final int id) {
		try {
			item.setId(id);
			Content updated = service.update(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CONTENTS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getContent")
	public Content getContent(@Auth User user, @NotNull @Valid final IdValueParams params) {
		return service.get(params.getId());
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/duplicate")
	@RolesAllowed({UserPermissions.CONTENTS_ADD})
	public Content duplicate(@Auth User user, @NotNull @Valid final IdValueParams params) {
		try {
			return service.duplicate(params.getId(), user);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
