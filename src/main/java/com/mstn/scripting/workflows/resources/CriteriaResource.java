/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.Criteria;
import com.mstn.scripting.workflows.core.CriteriaService;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a criterios.
 *
 * @author MSTN Media
 */
@Path("/criteria")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.CRITERIAS_LIST})
public class CriteriaResource {

	private final CriteriaService service;

	/**
	 *
	 * @param criteriaService
	 */
	public CriteriaResource(CriteriaService criteriaService) {
		this.service = criteriaService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Criteria>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) {
		try {
			Representation<List<Criteria>> result = service.getRepresentation(where, pageSize, pageNumber, orderBy, user);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Criteria>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			WhereClause whereClause = service.getUserWhere(user, where);
			List<Criteria> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Criteria>> get(@Auth User user, @PathParam("id") final int id) {
		Criteria item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.CRITERIAS_ADD})
	public Representation<List<Criteria>> create(@Auth User user, @NotNull @Valid final Criteria item) {
		Criteria inserted = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(inserted));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CRITERIAS_EDIT})
	public Representation<List<Criteria>> edit(
			@Auth User user,
			@NotNull @Valid final Criteria item,
			@PathParam("id") final int id) {
		item.setId(id);
		Criteria updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CRITERIAS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

}
