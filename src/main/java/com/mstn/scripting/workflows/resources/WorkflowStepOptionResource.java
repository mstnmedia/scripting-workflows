/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.workflows.core.WorkflowStepOptionService;
import com.mstn.scripting.workflows.core.WorkflowStepService;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a opciones de pasos.
 *
 * @author josesuero
 */
@Path("/workflowStepOption")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.WORKFLOWS_LIST})
public class WorkflowStepOptionResource {

	private final WorkflowStepService stepService;
	private final WorkflowStepOptionService service;

	/**
	 *
	 * @param stepService
	 * @param service
	 */
	public WorkflowStepOptionResource(WorkflowStepService stepService, WorkflowStepOptionService service) {
		this.service = service;
		this.stepService = stepService;
	}

	/**
	 *
	 * @param user
	 * @param id_step
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Workflow_Step_Option>> getAll(
			@Auth User user,
			@PathParam("id_version") final int id_step) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.getAll(id_step));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow_Step_Option>> get(@Auth User user, @PathParam("id") final int id) {
		Workflow_Step_Option item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.WORKFLOWS_ADD})
	public Representation<List<Workflow_Step_Option>> create(
			@Auth User user,
			@NotNull @Valid final Workflow_Step_Option item) throws Exception {
		try {
			Workflow_Step_Option created = service.create(item, stepService, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_EDIT})
	public Representation<List<Workflow_Step_Option>> edit(
			@Auth User user,
			@NotNull @Valid final Workflow_Step_Option item,
			@PathParam("id") final int id) throws Exception {
		try {
			item.setId(id);
			Workflow_Step_Option updated = service.update(item, stepService, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, stepService, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getOptionsByStepID")
	public List<Workflow_Step_Option> getOptionsByStepID(@Auth User user, @NotNull @Valid final IdValueParams params) {
		try {
			return service.getAll(params.getId());
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}
}
