/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.workflows.core.WorkflowService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a flujos.
 *
 * @author MSTN Media
 */
@Path("/workflow")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.WORKFLOWS_LIST})
public class WorkflowResource {

	private final WorkflowService service;

	/**
	 *
	 * @param workflowService
	 */
	public WorkflowResource(WorkflowService workflowService) {
		this.service = workflowService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Workflow>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			Representation<List<Workflow>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Workflow>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Workflow> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow>> get(@Auth User user, @PathParam("id") final int id) {
		Workflow item = service.get(id, true);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.WORKFLOWS_ADD})
	public Representation<List<Workflow>> create(
			@Auth User user,
			@NotNull @Valid final Workflow item) {
		try {
			Workflow created = service.create(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param item
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_EDIT})
	public Representation<List<Workflow>> edit(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final Workflow item) {
		try {
			item.setId(id);
			Workflow updated = service.update(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param config
	 * @return
	 */
	@POST
	@Timed
	@Path("/publish")
	@RolesAllowed({UserPermissions.WORKFLOWS_PUBLISH})
	public Representation<WorkflowPublishResponse> publish(
			@Auth User user,
			@NotNull @Valid final WorkflowPublishParams config
	) {
		try {
			WorkflowPublishResponse response = service.publish(config, user);
			return new Representation<>(HttpStatus.OK_200, response);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@POST
	@Timed
	@Path("/{id}/active")
	public Representation<List<Workflow>> getCurrentVersion(
			@Auth User user,
			@PathParam("id") final int id/*, @Auth User user*/) {
		Workflow item = service.getActive(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/active")
	public Workflow getCurrentVersion(@Auth User user, @NotNull @Valid final IdValueParams params) {
		return service.getActive(params.getId());
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param id_version
	 * @return
	 */
	@POST
	@Path("/{id}/version/{id_version}")
	public Representation<List<Workflow>> get(
			@Auth User user,
			@PathParam("id") final int id,
			@PathParam("id_version") final String id_version) {
		try {
			Workflow item = service.getWithVersion(id, id_version);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	@GET
	@Timed
	@Path("/workflowsWithCriterias")
	public List<Workflow> getAllWithCriterias(@Auth User user, @QueryParam("where") String where) {
		return postAllWithCriterias(user, where);
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@POST
	@Timed
	@Path("/workflowsWithCriterias")
	public List<Workflow> postAllWithCriterias(@Auth User user, @QueryParam("where") String where) {
		try {
			WhereClause whereClause = service.getUserWhere(user, Where.listFrom(where));
			List<Workflow> list = service.getAllWithCriterias(whereClause);
			return list;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}/export")
	public Workflow export(@PathParam("id") final int id, @Auth User user) {
		try {
			Workflow item = service.export(id, user);
			return item;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}/import")
	public WorkflowPublishResponse importWorkflow(@Auth User user, @NotNull @Valid final Workflow item) {
		try {
			WorkflowPublishResponse response = service.importWorkflow(item, user);
			return response;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
