/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.workflows.core.WorkflowVersionService;
import com.mstn.scripting.workflows.db.Workflow_Version;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a versiones de
 * flujo.
 *
 * @author MSTN Media
 */
@Path("/workflowVersion")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.CONTENTS_LIST})
public class WorkflowVersionResource {

	private final WorkflowVersionService service;

	/**
	 *
	 * @param versionService
	 */
	public WorkflowVersionResource(WorkflowVersionService versionService) {
		this.service = versionService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Workflow_Version>> getAll(
			@Auth User user, @QueryParam("where") String where) throws Exception {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Workflow_Version> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow_Version>> get(@Auth User user, @PathParam("id") final int id) {
		Workflow_Version item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

}
