package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Workflow_Criteria;
import com.mstn.scripting.workflows.core.WorkflowCriteriaService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a la relación
 * criterio-flujo.
 *
 * @author MSTN Media
 */
@Path("/workflowCriteria")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.WORKFLOWS_LIST})
public class WorkflowCriteriaResource {

	private final WorkflowCriteriaService service;

	/**
	 *
	 * @param service
	 */
	public WorkflowCriteriaResource(WorkflowCriteriaService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Workflow_Criteria>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Workflow_Criteria> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow_Criteria>> get(@Auth User user, @PathParam("id") final int id) {
		try {
			List<Workflow_Criteria> list = new ArrayList<>();
			list.add(service.get(id));
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.WORKFLOWS_ADD})
	public Representation<List<Workflow_Criteria>> create(
			@Auth User user,
			@NotNull @Valid final Workflow_Criteria item) {
		try {
			Workflow_Criteria inserted = service.create(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(inserted));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_EDIT})
	public Representation<List<Workflow_Criteria>> edit(
			@Auth User user,
			@NotNull @Valid final Workflow_Criteria item,
			@PathParam("id") final int id) {
		try {
			item.setId(id);
			Workflow_Criteria updated = service.update(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
