/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.workflows.core.WorkflowStepService;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a pasos.
 *
 * @author josesuero
 */
@Path("/workflowstep")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.WORKFLOWS_LIST})
public class WorkflowStepResource {

	private final WorkflowStepService service;

	/**
	 *
	 * @param workflowStepService
	 */
	public WorkflowStepResource(WorkflowStepService workflowStepService) {
		this.service = workflowStepService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Workflow_Step>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			Representation<List<Workflow_Step>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Workflow_Step>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Workflow_Step> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Workflow_Step>> get(@Auth User user, @PathParam("id") final int id) {
		Workflow_Step item = service.get(id, true);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param replaceInitial
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.WORKFLOWS_ADD})
	public Representation<List<Workflow_Step>> create(
			@Auth User user,
			@QueryParam("replaceInitial") boolean replaceInitial,
			@NotNull @Valid Workflow_Step item
	) {
		try {
			Workflow_Step created = service.create(item, user, replaceInitial);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_EDIT})
	public Representation<List<Workflow_Step>> edit(
			@Auth User user,
			@NotNull @Valid Workflow_Step item,
			@PathParam("id") int id) {
		try {
			item.setId(id);
			Workflow_Step updated = service.update(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.WORKFLOWS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getStep")
	public Workflow_Step getStep(@Auth User user, @NotNull @Valid IdValueParams params) {
		try {
			boolean loadChildren = Boolean.parseBoolean(params.getValue());
			return service.get(params.getId(), loadChildren);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getStepWithOptions")
	public Workflow_Step getStepWithOptions(
			@Auth User user,
			@NotNull @Valid IdValueParams params) {
		try {
			return service.getWithOptions(params.getId());
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getFirstStep")
	public Workflow_Step getFirstStep(
			@Auth User user,
			@NotNull @Valid IdValueParams params) {
		try {
			return service.getFirst(params.getId());
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getReferersOfInterface")
	public List<Workflow_Step> getReferersOfInterface(
			@Auth User user,
			@NotNull @Valid IdValueParams params) {
		try {
			boolean loadOptions = Boolean.parseBoolean(params.getValue());
			return service.getReferersOfInterface(params.getId(), loadOptions);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/updateInterfaceServiceReferers")
	public String updateInterfaceServiceReferers(
			@Auth User user,
			@NotNull @Valid IdValueParams params) {
		try {
			Interface_Service interfaceService = JSON.toObject(params.getValue(), Interface_Service.class);
			return service.updateReferers(interfaceService, user);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/workflowDependencies")
	public List<Workflow_Step> workflowDependencies(
			@Auth User user,
			@NotNull @Valid IdValueParams params) {
		try {
			List<Workflow_Step> list = service.workflowDependencies(params.getId(), user);
			return list;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}/getDependentSteps")
	public List<Workflow_Step> getDependentSteps(@Auth User user, @PathParam("id") int id) {
		try {
			List<Workflow_Step> list = service.getDependentSteps(id, user);
			return list;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@POST
	@Timed
	@Path("/{id}/getDependentSteps")
	public List<Workflow_Step> postDependentSteps(@Auth User user, @PathParam("id") int id) {
		try {
			return getDependentSteps(user, id);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/markAsInitial")
	public Workflow_Step markAsInitial(@Auth User user, @NotNull @Valid IdValueParams params) {
		try {
			Workflow_Step step = service.markAsInitial(params.getId(), user);
			return step;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
