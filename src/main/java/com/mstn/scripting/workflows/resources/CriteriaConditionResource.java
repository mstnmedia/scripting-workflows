/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Criteria_Condition;
import com.mstn.scripting.workflows.core.CriteriaConditionService;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a condiciones de
 * criterios.
 *
 * @author MSTN Media
 */
@Path("/criteriaCondition")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.CRITERIAS_LIST})
public class CriteriaConditionResource {

	private final CriteriaConditionService service;

	/**
	 *
	 * @param criteriaConditionService
	 */
	public CriteriaConditionResource(CriteriaConditionService criteriaConditionService) {
		this.service = criteriaConditionService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Criteria_Condition>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		try {
			List<Where> clientWhere = Where.listFrom(where);
			WhereClause whereClause = service.getUserWhere(user, clientWhere);
			List<Criteria_Condition> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Criteria_Condition>> get(@Auth User user, @PathParam("id") final int id) {
		Criteria_Condition item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.CRITERIAS_ADD})
	public Representation<List<Criteria_Condition>> create(@Auth User user, @NotNull @Valid final Criteria_Condition item) throws Exception {
		try {
			Criteria_Condition inserted = service.create(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(inserted));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CRITERIAS_EDIT})
	public Representation<List<Criteria_Condition>> edit(
			@Auth User user,
			@NotNull @Valid final Criteria_Condition item,
			@PathParam("id") final int id) {
		try {
			item.setId(id);
			Criteria_Condition updated = service.update(item, user);
			return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CRITERIAS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
