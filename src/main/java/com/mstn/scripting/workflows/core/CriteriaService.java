/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Criteria;
import com.mstn.scripting.core.models.Criteria_Condition;
import com.mstn.scripting.workflows.db.CriteriaDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con criterios.
 *
 * @author josesuero
 */
public abstract class CriteriaService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	CriteriaConditionService conditionService;

	/**
	 *
	 * @param conditionService
	 * @return
	 */
	public CriteriaService setConditionService(CriteriaConditionService conditionService) {
		this.conditionService = conditionService;
		return this;
	}

	@CreateSqlObject
	abstract CriteriaDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	public WhereClause getUserWhere(User user, String where) throws Exception {
		List<Where> clientWhere = Arrays.asList(JSON.toObject(where, Where[].class));
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Criteria item, Criteria before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Representation<List<Criteria>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy, User user) throws Exception {
		WhereClause whereClause = getUserWhere(user, where);
		long totalRows = getCount(whereClause);
		List<Criteria> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(HttpStatus.OK_200, items, pageSize, pageNumber, totalRows);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Criteria> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return setConditions(dao().getAll(where, pageSize, pageNumber, orderBy));
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Criteria> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return setConditions(dao().getAll(where.getPreparedString(), where));
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Criteria get(int id) {
		Criteria item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		item.setConditions(conditionService.getAll(item.getId()));
		return item;
	}

	List<Criteria> setConditions(List<Criteria> list) {
		if (Utils.isNullOrEmpty(list)) {
			return list;
		}
		HashMap<Integer, Criteria> mapCriterias = new HashMap();
		List<Integer> criteriaIDs = list.stream()
				.map(item -> {
					item.setConditions(new ArrayList());
					mapCriterias.put(item.getId(), item);
					return item.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_criteria", Where.IN, criteriaIDs));
		List<Criteria_Condition> conditions = conditionService.getAll(where);
		conditions.forEach(c -> {
			Criteria criteria = mapCriterias.get(c.getId_criteria());
			criteria.getConditions().add(c);
		});
		return list;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Criteria create(Criteria item, User user) {
		validateItemCenter(item, null, user);
		int id = dao().insert(item);
		item.setId(id);
		log().insert(user.getId(), "Criterios", id, "Crear", item, item, user.getIp());
		List<Criteria_Condition> conditions = item.getConditions();
		conditions.forEach(condition -> {
			condition.setId_center(item.getId_center());
			condition.setId_criteria(item.getId());
			conditionService.create(condition, user);
		});
		return get(id);
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Criteria update(Criteria item, User user) {
		Criteria before = get(item.getId());
		validateItemCenter(item, before, user);
		dao().update(item);
		log().insert(user.getId(), "Criterios", before.getId(), "Actualizar", before, item, user.getIp());
		List<Criteria_Condition> oldConditions = conditionService.getAll(item.getId());
		List<Criteria_Condition> newConditions = item.getConditions();

		for (int i = 0; i < oldConditions.size(); i++) {
			Criteria_Condition oldC = oldConditions.get(i);
			boolean found = false;
			for (int j = 0; j < newConditions.size(); j++) {
				Criteria_Condition newC = newConditions.get(j);
				boolean match = oldC != null && oldC.getName().equals(newC.getName());
				if (match) {
					newC.setId(oldC.getId());
					newC.setId_center(oldC.getId_center());
					newC.setId_criteria(oldC.getId_criteria());
					conditionService.update(newC, user);
					newConditions.remove(j);
					found = true;
					break;
				}
			}
			if (!found) {
				conditionService.delete(oldC.getId(), user);
			}
		}
		for (int j = 0; j < newConditions.size(); j++) {
			Criteria_Condition newC = newConditions.get(j);
			newC.setId_center(item.getId_center());
			newC.setId_criteria(item.getId());
			conditionService.create(newC, user);
		}

		return get(item.getId());
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Criteria before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Criterios", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Response.Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
