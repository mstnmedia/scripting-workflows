/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import com.mstn.scripting.core.enums.WorkflowTypes;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.workflows.db.WorkflowDao;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.workflows.db.Workflow_StepDao;
import com.mstn.scripting.workflows.db.Workflow_Step_OptionDao;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con las opciones de los pasos.
 *
 * @author josesuero
 */
public abstract class WorkflowStepOptionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract Workflow_Step_OptionDao workflow_Step_OptionDao();

	@CreateSqlObject
	abstract Workflow_StepDao workflow_StepDao();

	@CreateSqlObject
	abstract WorkflowDao workflowDao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Workflow_Step_Option item, Workflow_Step_Option before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param id_step
	 * @return
	 */
	public List<Workflow_Step_Option> getAll(int id_step) {
		return getAll(id_step, false);
	}

	/**
	 *
	 * @param id_step
	 * @param loadParent
	 * @return
	 */
	public List<Workflow_Step_Option> getAll(int id_step, boolean loadParent) {
		List<Workflow_Step_Option> list = workflow_Step_OptionDao().getAll(id_step);
		if (!loadParent) {
			return list;
		}
		for (int i = 0; i < list.size(); i++) {
			Workflow_Step_Option item = list.get(i);
			if (item.getId_type() == WorkflowStepOptionTypes.STEP) {
				item.setReferedWorkflowStep(workflow_StepDao().get(Integer.parseInt(item.getValue())));
			}
		}
		return list;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Step_Option get(int id) {
		Workflow_Step_Option item = workflow_Step_OptionDao().get(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 */
	public Workflow_Step_Option get(int id, boolean loadChildren) {
		Workflow_Step_Option item = get(id);
		if (loadChildren && item.getId_type() == WorkflowStepOptionTypes.STEP) {
			Workflow_Step step = workflow_StepDao().get(Integer.parseInt(item.getValue()));
			item.setReferedWorkflowStep(step);
		}

		return item;
	}

	void validateDuplicates(Workflow_Step_Option item) throws Exception {
		//int id_step, String value, int id_option) {
		if (item.getId_type() == WorkflowStepOptionTypes.VALUE) {
			List<Workflow_Step_Option> options = getAll(item.getId_workflow_step());
			Stream<Workflow_Step_Option> matches = options
					.stream()
					.filter(
							option -> option.getId() != item.getId()
							&& option.getId_type() == WorkflowStepOptionTypes.VALUE
							&& Utils.stringAreEquals(item.getValue(), option.getValue()));
			if (matches.count() > 0) {
				throw new WebApplicationException(
						"Ya este paso contiene una opción con el valor " + item.getValue(),
						Response.Status.CONFLICT);
			}
		}
	}

	/**
	 *
	 * @param item
	 * @param stepService
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Workflow_Step_Option create(Workflow_Step_Option item, WorkflowStepService stepService, User user) throws Exception {
		validateItemCenter(item, null, user);
		Workflow_Step step = stepService.get(item.getId_workflow_step());
		item.setId(0);
		item.setId_center(step.getId_center());
		validateDuplicates(item);
		int id = workflow_Step_OptionDao().insert(item);
		Workflow_Step_Option actualOption = get(id);
		log().insert(user.getId(), "Opciones de pasos de flujos", id, "Crear", item, actualOption, user.getIp());

		Workflow workflow = workflowDao().get(actualOption.getId_workflow());

		if (workflow.getMain() == WorkflowTypes.SUB_WORKFLOW.getValue()
				&& actualOption.getId_type() == WorkflowStepOptionTypes.VALUE) {
			List<Workflow_Step> referers = stepService.getReferersOfSubWorkflow(workflow.getId(), true);
			for (int i = 0; i < referers.size(); i++) {
				Workflow_Step referer = referers.get(i);
				List<Workflow_Step_Option> options = referer.getWorkflow_step_option();
				boolean matches = false;
				for (Workflow_Step_Option option : options) {
					if (Utils.stringAreEquals(option.getName(), actualOption.getValue())) {
						matches = true;
						break;
					}
				}
				if (!matches) {
					Workflow_Step_Option newOption = new Workflow_Step_Option(
							0, referer.getId_center(), referer.getId_workflow(), referer.getId_workflow_version(),
							referer.getId(), referer.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
							actualOption.getValue(), actualOption.getColor(), "", actualOption.getOrder_index()
					);
					workflow_Step_OptionDao().insert(newOption);
				}
			}
		}
		return actualOption;
	}

	/**
	 *
	 * @param item
	 * @param stepService
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Workflow_Step_Option update(Workflow_Step_Option item, WorkflowStepService stepService, User user) throws Exception {
		Workflow_Step_Option before = get(item.getId());
		validateItemCenter(item, before, user);
		if (!user.isMaster() || item.getId_center() == 0) {
			item.setId_center(before.getId_center());
		}
		validateDuplicates(item);
		workflow_Step_OptionDao().update(item);
		Workflow_Step_Option actualOption = get(item.getId());
		log().insert(user.getId(), "Opciones de pasos de flujos", before.getId(), "Actualizar", before, actualOption, user.getIp());

		Workflow workflow = workflowDao().get(actualOption.getId_workflow());

		boolean isSubWorkflow = workflow.getMain() == WorkflowTypes.SUB_WORKFLOW.getValue();
		boolean optionChangeValue = actualOption.getId_type() == WorkflowStepOptionTypes.VALUE && !Utils.stringAreEquals(actualOption.getValue(), before.getValue());
		boolean optionChangeTypeFromValue = before.getId_type() != actualOption.getId_type()
				&& actualOption.getId_type() == WorkflowStepOptionTypes.STEP;
		if (isSubWorkflow && (optionChangeValue || optionChangeTypeFromValue)) {
			List<Workflow_Step> steps = stepService.getReferersOfSubWorkflow(workflow.getId(), true);
			for (int i = 0; i < steps.size(); i++) {
				Workflow_Step step = steps.get(i);
				List<Workflow_Step_Option> options = step.getWorkflow_step_option();
				for (Workflow_Step_Option option : options) {
					if (Utils.stringAreEquals(option.getName(), before.getValue())) {
						int timesValueIsReturned = workflowDao().timesWorkflowReturnValue(workflow.getId(), before.getValue());
						if (timesValueIsReturned == 0) {
							workflow_Step_OptionDao().delete(option.getId());
						}
						break;
					}
				}
				if (optionChangeValue) {
					List<Workflow_Step_Option> matches = options.stream()
							.filter(option -> Utils.stringAreEquals(option.getName(), actualOption.getValue()))
							.collect(Collectors.toList());
					if (matches.isEmpty()) {
						workflow_Step_OptionDao().insert(new Workflow_Step_Option(
								0, step.getId_center(), step.getId_workflow(), step.getId_workflow_version(),
								step.getId(), step.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
								actualOption.getValue(), actualOption.getColor(), "", actualOption.getOrder_index()
						));
					}
				}
			}
		}
		return actualOption;
	}

	/**
	 *
	 * @param id
	 * @param stepService
	 * @param user
	 * @return
	 */
	public String delete(final int id, WorkflowStepService stepService, User user) {
		Workflow_Step_Option before = get(id);
		int result = workflow_Step_OptionDao().delete(id);
		switch (result) {
			case 1: {
				Workflow workflow = workflowDao().get(before.getId_workflow());
				if (workflow.getMain() == WorkflowTypes.SUB_WORKFLOW.getValue()) {
					List<Workflow_Step> steps = stepService.getReferersOfSubWorkflow(workflow.getId(), true);
					for (int i = 0; i < steps.size(); i++) {
						Workflow_Step step = steps.get(i);
						List<Workflow_Step_Option> options = step.getWorkflow_step_option();

						for (Workflow_Step_Option option : options) {
							if (Utils.stringAreEquals(option.getName(), before.getValue())) {
								int timesValueIsReturned = workflowDao().timesWorkflowReturnValue(workflow.getId(), before.getValue());
								if (timesValueIsReturned == 0) {
									workflow_Step_OptionDao().delete(option.getId());
								}
								break;
							}
						}
					}
				}
				log().insert(user.getId(), "Opciones de pasos de flujos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			}
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			workflow_Step_OptionDao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
