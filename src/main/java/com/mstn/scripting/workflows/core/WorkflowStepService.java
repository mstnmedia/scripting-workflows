/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import com.mstn.scripting.core.enums.WorkflowStepTypes;
import com.mstn.scripting.core.enums.WorkflowVersionTypes;
import com.mstn.scripting.core.models.Content;
//import com.mstn.scripting.core.models.Content_Language;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import com.mstn.scripting.core.models.Workflow_Step_Interface_Field;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import com.mstn.scripting.workflows.ScriptingWorkflowsConfiguration;
import com.mstn.scripting.workflows.db.ContentDao;
import com.mstn.scripting.workflows.db.Content_LanguageDao;
import com.mstn.scripting.workflows.db.WorkflowDao;
import com.mstn.scripting.workflows.db.Workflow_StepDao;
import com.mstn.scripting.workflows.db.Workflow_Step_ContentDao;
import com.mstn.scripting.workflows.db.Workflow_Step_Interface_FieldDao;
import com.mstn.scripting.workflows.db.Workflow_Step_OptionDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.apache.http.client.HttpClient;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con los pasos de los flujos.
 *
 * @author josesuero
 */
public abstract class WorkflowStepService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "No se puede eliminar el paso %s. Tiene dependientes.";
	private static final String FORBIDDEN = "No tienes acceso a este registro #%s.";
	private static final String STEP_TYPE_IS_REQUIRED = "No especificó el tipo de paso. Este campo es requerido.";
	private static final String CANT_DELETE_SYSTEM_ITEM = "item id %s can't be deleted for a good behavior of system.";
	private static final String CANT_UPDATE_INACTIVE_VERSION = "Only inactive workflow versions can be modified.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public WorkflowStepService() {
	}

	private ScriptingWorkflowsConfiguration config;

	/**
	 *
	 * @return
	 */
	public ScriptingWorkflowsConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public WorkflowStepService setConfig(ScriptingWorkflowsConfiguration config) {
		this.config = config;
		return this;
	}

	private HttpClient httpClient;

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public WorkflowStepService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}
	
	private WorkflowService workflowService;

	public WorkflowStepService setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
		return this;
	}

	@CreateSqlObject
	abstract Workflow_StepDao dao();

	@CreateSqlObject
	abstract WorkflowDao workflowDao();

	@CreateSqlObject
	abstract Workflow_Step_ContentDao workflow_Step_ContentDao();

	@CreateSqlObject
	abstract Workflow_Step_OptionDao workflow_Step_OptionDao();

	@CreateSqlObject
	abstract Workflow_Step_Interface_FieldDao workflow_Step_Interface_FieldDao();

	@CreateSqlObject
	abstract ContentDao contentDao();

	@CreateSqlObject
	abstract Content_LanguageDao contentLanguageDao();

	@CreateSqlObject
	abstract Workflow_Step_OptionDao workflow_Step_OptionStructureDao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Workflow_Step item, Workflow_Step before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Workflow_Step>> getRepresentation(WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Workflow_Step> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(HttpStatus.OK_200, items, pageSize, pageNumber, totalRows);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow_Step> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow_Step> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_version
	 * @return
	 */
	public List<Workflow_Step> getAll(int id_version) {
		return dao().getAll(id_version);
	}

	/**
	 *
	 * @param id_version
	 * @param loadChildren
	 * @return
	 */
	public List<Workflow_Step> getAll(int id_version, boolean loadChildren) {
		return getAll(id_version, loadChildren, false);
	}

	/**
	 *
	 * @param id_version
	 * @param loadChildren
	 * @param loadContent
	 * @return
	 */
	public List<Workflow_Step> getAll(int id_version, boolean loadChildren, boolean loadContent) {
		List<Workflow_Step> steps = getAll(id_version);
		if (loadChildren && !steps.isEmpty()) {
			loadOptions(steps, new WhereClause(new Where("wv.id", id_version)));
			loadFields(steps);
			loadContents(steps, loadContent);
		}
		return steps;
	}

	/**
	 *
	 * @param id_step
	 * @param user
	 * @return
	 */
	public List<Workflow_Step> getDependentSteps(int id_step, User user) {
		Workflow_Step step = get(id_step);
		if (!user.isMaster() && user.getCenterId() != step.getId_center()) {
			throw new WebApplicationException(String.format(FORBIDDEN, id_step), HttpStatus.FORBIDDEN_403);
		}
		List<Workflow_Step> dependents = dao().getDependentSteps(id_step);
		return dependents;
	}

	/**
	 *
	 * @param id_subworkflow
	 * @return
	 */
	public List<Workflow_Step> getReferersOfSubWorkflow(int id_subworkflow) {
		return getReferersOfSubWorkflow(id_subworkflow, false);
	}

	/**
	 *
	 * @param id_subworkflow
	 * @param loadOptions
	 * @return
	 */
	public List<Workflow_Step> getReferersOfSubWorkflow(int id_subworkflow, boolean loadOptions) {
		List<Workflow_Step> steps = dao().getAllByTypeAndValue(WorkflowStepTypes.WORKFLOW.getValue(), id_subworkflow);
		if (loadOptions) {
			loadOptions(steps, new WhereClause(
					new Where("ws.id_type", WorkflowStepTypes.WORKFLOW.getValue()),
					new Where("ws.id_workflow_external", id_subworkflow),
					new Where("wv.active", 0)
			));
		}
		return steps;
	}

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	public List<Workflow_Step> getReferersOfInterface(int id_interface) {
		return getReferersOfInterface(id_interface, true);
	}

	/**
	 *
	 * @param id_interface
	 * @param loadOptions
	 * @return
	 */
	public List<Workflow_Step> getReferersOfInterface(int id_interface, boolean loadOptions) {
		List<Workflow_Step> steps = dao().getAllByTypeAndValue(WorkflowStepTypes.EXTERNAL.getValue(), id_interface);
		if (loadOptions) {
			loadOptions(steps, new WhereClause(
					new Where("ws.id_type", WorkflowStepTypes.EXTERNAL.getValue()),
					new Where("ws.id_workflow_external", id_interface),
					new Where("wv.active", 0)
			));
		}
		return steps;
	}

	/**
	 *
	 * @param id_service
	 * @return
	 */
	public List<Workflow_Step> getReferersOfInterfaceService(int id_service) {
		return getReferersOfInterfaceService(id_service, true);
	}

	/**
	 *
	 * @param id_service
	 * @param loadOptions
	 * @return
	 */
	public List<Workflow_Step> getReferersOfInterfaceService(int id_service, boolean loadOptions) {
		List<Workflow_Step> steps = dao().getAllByInterfaceService(id_service);
		if (loadOptions) {
			loadOptions(steps, new WhereClause(
					new Where("ws.id_type", WorkflowStepTypes.EXTERNAL.getValue()),
					new Where("ws.id_interface_service", id_service),
					new Where("wv.active", 0)
			));
		}
		return steps;
	}

	/**
	 *
	 * @param id_version
	 * @return
	 */
	public Workflow_Step getFirst(int id_version) {
		Workflow_Step item = dao().getFirst(id_version);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id_version), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Step get(int id) {
		Workflow_Step item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 */
	public Workflow_Step get(int id, boolean loadChildren) {
		Workflow_Step item = get(id);
		if (loadChildren) {
			item.setWorkflow_step_option(getAllOption(item.getId(), true));
			if (item.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
				item.setWorkflow(workflowDao().get(item.getId_workflow_external()));
			} else if (item.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
				item.setWorkflow_step_interface_field(getAllFields(item.getId()));
			}
			if (item.getId_type() == WorkflowStepTypes.REGULAR.getValue()
					|| item.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
				item.setWorkflow_step_content(getAllContent(item.getId(), true));
			}
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Step getWithOptions(int id) {
		Workflow_Step item = get(id);
		item.setWorkflow_step_option(getAllOption(item.getId(), false));
		return item;
	}

	/**
	 *
	 * @param id_step
	 * @param loadContent
	 * @return
	 */
	public List<Workflow_Step_Content> getAllContent(int id_step, boolean loadContent) {
		List<Workflow_Step_Content> list = workflow_Step_ContentDao().getAll(id_step);
		if (loadContent && !list.isEmpty()) {
			List<Integer> contentIDs = list.stream()
					.map(item -> item.getId_content())
					.collect(Collectors.toList());
			WhereClause where = new WhereClause(new Where("id", Where.IN, contentIDs));
			List<Content> referedContents = contentDao().getAll(where.getPreparedString(), where);
			HashMap<Integer, Content> mapRefereds = new HashMap();
			referedContents.forEach(referer -> mapRefereds.put(referer.getId(), referer));

//			WhereClause whereL = new WhereClause(new Where("id_content", Where.IN, contentIDs));
//			List<Content_Language> languages = contentLanguageDao().getAll(whereL.getPreparedString(), whereL);
//			languages.forEach(language -> {
//				Content content = mapRefereds.getOrDefault(language.getId_content(), new Content());
//				if (content.getContent_language() == null) {
//					content.setContent_language(new ArrayList());
//				}
//				content.getContent_language().add(language);
//			});
			list.forEach(item -> item.setContent(mapRefereds.get(item.getId_content())));
		}
		return list;
	}

	/**
	 *
	 * @param id_step
	 * @param loadReferedStep
	 * @return
	 */
	public List<Workflow_Step_Option> getAllOption(int id_step, boolean loadReferedStep) {
		List<Workflow_Step_Option> list = workflow_Step_OptionDao().getAll(id_step);
		if (loadReferedStep && !list.isEmpty()) {
			List<Integer> stepIDs = list.stream()
					.filter(item -> item.getId_type() == WorkflowStepOptionTypes.STEP)
					.map(item -> Integer.parseInt(item.getValue()))
					.collect(Collectors.toList());
			List<Workflow_Step> referedSteps = getAll(new WhereClause(new Where("id", Where.IN, stepIDs)));
			HashMap<Integer, Workflow_Step> mapRefereds = new HashMap();
			referedSteps.forEach(referer -> mapRefereds.put(referer.getId(), referer));
			list.forEach(option -> {
				if (option.getId_type() == WorkflowStepOptionTypes.STEP) {
					option.setReferedWorkflowStep(mapRefereds.get(Integer.parseInt(option.getValue())));
				}
			});
		}
		return list;
	}

	/**
	 *
	 * @param id_step
	 * @return
	 */
	public List<Workflow_Step_Interface_Field> getAllFields(int id_step) {
		List<Workflow_Step_Interface_Field> list = workflow_Step_Interface_FieldDao().getAll(id_step);
		return list;
	}

	/**
	 *
	 * @param steps
	 * @param stepWhere
	 * @return
	 */
	public List<Workflow_Step> loadOptions(List<Workflow_Step> steps, WhereClause stepWhere) {
		if (Utils.isNullOrEmpty(steps)) {
			return steps;
		}
//		List<Integer> stepIDs = steps.stream()
//				.map(item -> {
//					item.setWorkflow_step_option(new ArrayList());
//					return item.getId();
//				})
//				.collect(Collectors.toList());
		steps.forEach(step -> step.setWorkflow_step_option(new ArrayList()));
		HashMap<Integer, Workflow_Step> mapSteps = new HashMap();
		steps.forEach(step -> mapSteps.put(step.getId(), step));
//		WhereClause where = new WhereClause(new Where("id_workflow_step", Where.IN, stepIDs));
//		List<Workflow_Step_Option> options = workflow_Step_OptionDao().getAll(where.getPreparedString(), where);
		List<Workflow_Step_Option> options = workflow_Step_OptionDao().getAllJoin(stepWhere.getPreparedString(), stepWhere);
		options.forEach(option -> {
			Workflow_Step step = mapSteps.get(option.getId_workflow_step());
			step.getWorkflow_step_option().add(option);
		});

		return steps;
	}

	public List<Workflow_Step> loadContents(List<Workflow_Step> steps, boolean loadContent) {
		if (Utils.isNullOrEmpty(steps)) {
			return steps;
		}
		HashMap<Integer, Workflow_Step> mapSteps = new HashMap();
		List<Integer> stepIDs = steps.stream()
				.filter(step -> step.getId_type() != WorkflowStepTypes.WORKFLOW.getValue())
				.map(step -> {
					mapSteps.put(step.getId(), step);
					step.setWorkflow_step_content(new ArrayList());
					return step.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_workflow_step", Where.IN, stepIDs));
		List<Workflow_Step_Content> WSCs = workflow_Step_ContentDao().getAll(where.getPreparedString(), where);
		WSCs.forEach(wsc -> {
			Workflow_Step step = mapSteps.get(wsc.getId_workflow_step());
			step.getWorkflow_step_content().add(wsc);
		});
		if (loadContent) {
			List<Integer> contentIDs = WSCs.stream()
					.map(item -> item.getId_content())
					.collect(Collectors.toList());
			WhereClause whereC = new WhereClause(new Where("id", Where.IN, contentIDs));
			HashMap<Integer, Content> mapContents = new HashMap();
			List<Content> contents = contentDao().getAll(whereC.getPreparedString(), whereC);
			contents.forEach(referer -> mapContents.put(referer.getId(), referer));
			WSCs.forEach(wsc -> wsc.setContent(mapContents.get(wsc.getId_content())));
		}
		return steps;
	}

	public List<Workflow_Step> loadFields(List<Workflow_Step> steps) {
		if (Utils.isNullOrEmpty(steps)) {
			return steps;
		}
		HashMap<Integer, Workflow_Step> mapSteps = new HashMap();
		List<Integer> stepIDs = steps.stream()
				.filter(step -> step.getId_type() == WorkflowStepTypes.EXTERNAL.getValue())
				.map(step -> {
					mapSteps.put(step.getId(), step);
					step.setWorkflow_step_interface_field(new ArrayList());
					return step.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_workflow_step", Where.IN, stepIDs));
		List<Workflow_Step_Interface_Field> WSIFs = workflow_Step_Interface_FieldDao().getAll(where.getPreparedString(), where);
		WSIFs.forEach(field -> {
			Workflow_Step step = mapSteps.get(field.getId_workflow_step());
			step.getWorkflow_step_interface_field().add(field);
		});
		return steps;
	}

	/**
	 *
	 * @param id_workflow_version
	 * @return
	 */
	public List<Workflow_Step_Option> getValueOptionsOfSubWorkflow(int id_workflow_version) {
		WhereClause where = new WhereClause(
				//new Where("id_workflow", Integer.toString(subWorkflow.getId())),
				new Where("id_workflow_version", id_workflow_version),
				new Where("id_type", Integer.toString(WorkflowStepOptionTypes.VALUE))
		);
		return workflow_Step_OptionDao().getAll(where.getPreparedString(), where);
	}

	private void createOptionsFromSubWorkflow(Workflow_Step item) {
		Workflow subWorkflow = workflowDao().get(item.getId_workflow_external());
		List<Workflow_Step_Option> subOptions = getValueOptionsOfSubWorkflow(subWorkflow.getVersion_id());
		HashMap<String, Workflow_Step_Option> values = new HashMap<>();
		for (int i = 0; i < subOptions.size(); i++) {
			Workflow_Step_Option option = subOptions.get(i);
			if (Objects.nonNull(option.getValue())) {
				values.put(option.getValue(), option);
			}
		}
		values.forEach((value, option) -> {
			Workflow_Step_Option newOption = new Workflow_Step_Option(
					0, item.getId_center(), item.getId_workflow(), item.getId_workflow_version(),
					item.getId(), item.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
					value, "", "", option.getOrder_index()
			);
			workflow_Step_OptionDao().insert(newOption);
		});
	}

	private void updateOptionsFromSubWorkflow(Workflow_Step item) {
		Workflow subWorkflow = workflowDao().get(item.getId_workflow_external());
		List<Workflow_Step_Option> currentOptions = workflow_Step_OptionDao().getAll(item.getId());
		List<Workflow_Step_Option> subOptions = getValueOptionsOfSubWorkflow(subWorkflow.getVersion_id());
		List<String> usedValues = new ArrayList();
		for (int i = 0; i < subOptions.size(); i++) {
			Workflow_Step_Option subOption = subOptions.get(i);
			String name = subOption.getValue();
			if (usedValues.contains(name)) {
				continue;
			}
			boolean found = false;
			for (int j = 0; j < currentOptions.size(); j++) {
				Workflow_Step_Option option = currentOptions.get(j);
				boolean match = Utils.stringAreEquals(option.getName(), name);
				if (match) {
					option.setColor(subOption.getColor());
					option.setOrder_index(subOption.getOrder_index());
					workflow_Step_OptionDao().update(option);
					currentOptions.remove(j);
					found = true;
					break;
				}
			}
			if (!found) {
				workflow_Step_OptionDao().insert(new Workflow_Step_Option(
						0, item.getId_center(), item.getId_workflow(), item.getId_workflow_version(),
						item.getId(), item.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
						name, subOption.getColor(), "", subOption.getOrder_index()
				));
			}
			usedValues.add(name);
		}
		for (int j = 0; j < currentOptions.size(); j++) {
			Workflow_Step_Option option = currentOptions.get(j);
			workflow_Step_OptionDao().delete(option.getId());
		}
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Workflow_Step create(Workflow_Step item, User user) throws Exception {
		return create(item, user, false);
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @param replaceInitial
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Workflow_Step create(Workflow_Step item, User user, boolean replaceInitial) throws Exception {
		validateItemCenter(item, null, user);
		Workflow workflow = workflowDao().get(item.getId_workflow());
		item.setId_center(workflow.getId_center());
		if (Objects.isNull(workflow)) {
			throw new WebApplicationException(String.format(NOT_FOUND, item.getId_workflow()), Response.Status.NOT_FOUND);
		}
		item.setId_workflow_version(workflow.getVersion_id());
		item.setInitial_step(replaceInitial || false);

		Workflow_Step first = null;
		if (replaceInitial) {
			first = getFirst(item.getId_workflow_version());
		}
		int id = createNoTransaction(item, dao(), workflow_Step_ContentDao(), user);
		if (replaceInitial && first != null) {
			delete(first.getId(), user, true);
		}
		Workflow_Step after = get(id, true);
		log().insert(user.getId(), "Pasos de flujos", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param workflowStepDao
	 * @param workflowStepContentDao
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int createNoTransaction(Workflow_Step item,
			Workflow_StepDao workflowStepDao,
			Workflow_Step_ContentDao workflowStepContentDao,
			User user) throws Exception {
		String GUID = java.util.UUID.randomUUID().toString();
		item.setGuid(GUID);
		// TODO: Constraint to allow only one initial step for each step
		int idStep = workflowStepDao.insert(item);
		item.setId(idStep);
		if (item.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
			createOptionsFromSubWorkflow(item);
		} else if (item.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
			List<String> valueList = Interface_Service.getValues(item.getId_interface_service(), httpClient, config.getApiURL(), user.getToken());
			for (int i = 0; i < valueList.size(); i++) {
				String value = valueList.get(i);

				Workflow_Step_Option newOption = new Workflow_Step_Option(
						0, item.getId_center(), item.getId_workflow(), item.getId_workflow_version(),
						item.getId(), item.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
						value, "", "", i
				);
				workflow_Step_OptionDao().insert(newOption);
			}

			List<Workflow_Step_Interface_Field> WSIFs = item.getWorkflow_step_interface_field();
			WSIFs.forEach((wsif) -> {
				wsif.setId_center(item.getId_center());
				wsif.setId_workflow_step(idStep);
				int insertID = workflow_Step_Interface_FieldDao().insert(wsif);
			});
		}
		if (Utils.listNonNullOrEmpty(item.getWorkflow_step_content())
				&& (item.getId_type() == WorkflowStepTypes.REGULAR.getValue()
				|| item.getId_type() == WorkflowStepTypes.EXTERNAL.getValue())) {
			for (Workflow_Step_Content stepContent : item.getWorkflow_step_content()) {
				if (stepContent != null) {
					workflowStepContentDao.insert(stepContent, item);
				}
			}
		}
		return idStep;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Workflow_Step update(Workflow_Step item, User user) throws Exception {
		Workflow_Step before = get(item.getId(), true);
		validateItemCenter(item, before, user);
		item.setId_center(before.getId_center());
		if (item.getId_type() == WorkflowStepTypes.UNDEFINED.getValue()) {
			throw new WebApplicationException(STEP_TYPE_IS_REQUIRED, HttpStatus.NOT_ACCEPTABLE_406);
		}
		if (before.getId_type() != item.getId_type()) {
			if (before.getId_type() == WorkflowStepTypes.REGULAR.getValue()) {
				deleteOptions(item, user);
			}
		}
		if (item.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
			if (item.getId_workflow_external() != before.getId_workflow_external()) {
				updateOptionsFromSubWorkflow(item);
			}
		} else if (item.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
			List<String> serviceValues = Interface_Service.getValues(item.getId_interface_service(), httpClient, config.getApiURL(), user.getToken());
			List<Workflow_Step_Option> currentOptions = workflow_Step_OptionDao().getAll(item.getId());

			for (int i = 0; i < serviceValues.size(); i++) {
				String value = serviceValues.get(i);
				boolean found = false;
				for (int j = 0; j < currentOptions.size(); j++) {
					Workflow_Step_Option option = currentOptions.get(j);
					boolean match = value != null && value.equals(option.getName());
					if (match) {
						option.setOrder_index(i);
						workflow_Step_OptionDao().update(option);
						currentOptions.remove(j);
						found = true;
						break;
					}
				}
				if (!found) {
					workflow_Step_OptionDao().insert(new Workflow_Step_Option(
							0, item.getId_center(), item.getId_workflow(), item.getId_workflow_version(),
							item.getId(), item.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
							value, "", "", i
					));
				}
			}
			for (int j = 0; j < currentOptions.size(); j++) {
				Workflow_Step_Option option = currentOptions.get(j);
				workflow_Step_OptionDao().delete(option.getId());
			}

			List<Workflow_Step_Interface_Field> currentWSIFs = getAllFields(item.getId());
			List<Workflow_Step_Interface_Field> WSIFs = item.getWorkflow_step_interface_field();
			for (int i = 0; i < currentWSIFs.size(); i++) {
				Workflow_Step_Interface_Field currentWSIF = currentWSIFs.get(i);

				boolean found = false;
				for (int j = 0; j < WSIFs.size(); j++) {
					Workflow_Step_Interface_Field wsif = WSIFs.get(j);
					boolean match = currentWSIF.getId_interface() == wsif.getId_interface()
							&& Utils.stringAreEquals(currentWSIF.getId_interface_field(), wsif.getId_interface_field());
					if (match) {
						wsif.setId(currentWSIF.getId());
						workflow_Step_Interface_FieldDao().update(wsif);
						WSIFs.remove(j);
						found = true;
						break;
					}
				}
				if (!found) {
					workflow_Step_Interface_FieldDao().delete(currentWSIF.getId());
				}
			}
			for (int j = 0; j < WSIFs.size(); j++) {
				Workflow_Step_Interface_Field wsif = WSIFs.get(j);
				workflow_Step_Interface_FieldDao().insert(wsif);
			}
		}
		dao().update(item);

		Workflow_Step after = get(item.getId(), true);
		log().insert(user.getId(), "Pasos de flujos", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param service
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public String updateReferers(Interface_Service service, User user) throws Exception {
		List<Workflow_Step> serviceReferers = getReferersOfInterfaceService(service.getId(), true);
		for (int idx = 0; idx < serviceReferers.size(); idx++) {
			Workflow_Step item = serviceReferers.get(idx);
			if (item.getId_interface_service() == service.getId()) {
				List<String> serviceValues = Utils.splitAsList(service.getReturned_values());
				List<Workflow_Step_Option> currentOptions = item.getWorkflow_step_option();

				for (int i = 0; i < serviceValues.size(); i++) {
					String value = serviceValues.get(i);
					boolean found = false;
					for (int j = 0; j < currentOptions.size(); j++) {
						Workflow_Step_Option option = currentOptions.get(j);
						boolean match = value != null && value.equals(option.getName());
						if (match) {
							option.setOrder_index(i);
							workflow_Step_OptionDao().update(option);
							currentOptions.remove(j);
							found = true;
							break;
						}
					}
					if (!found) {
						workflow_Step_OptionDao().insert(new Workflow_Step_Option(
								0, item.getId_center(), item.getId_workflow(), item.getId_workflow_version(),
								item.getId(), item.getGuid(), WorkflowStepOptionTypes.UNDEFINED,
								value, "", "", i
						));
					}
				}
				for (int j = 0; j < currentOptions.size(); j++) {
					Workflow_Step_Option option = currentOptions.get(j);
					workflow_Step_OptionDao().delete(option.getId());
				}

				dao().update(item);
				Workflow_Step after = get(item.getId(), true);
				log().insert(user.getId(), "Pasos de flujos", item.getId(), "Actualizar", item, after, user.getIp());
			}
		}
		return "true";
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		return delete(id, user, false);
	}

	/**
	 *
	 * @param step
	 * @param user
	 */
	public void deleteOptions(Workflow_Step step, User user) {
		List<Workflow_Step_Option> optionsForDelete = getAllOption(step.getId(), false);
//		optionsForDelete.removeIf(option -> option.getId_type() != WorkflowStepOptionTypes.VALUE);
		if (!optionsForDelete.isEmpty()) {
			List<Workflow_Step> referrerSteps = getReferersOfSubWorkflow(step.getId_workflow(), true);
			for (int sIdx = 0; sIdx < referrerSteps.size(); sIdx++) {
				Workflow_Step referrerStep = referrerSteps.get(sIdx);
				List<Workflow_Step_Option> referrerOptions = Utils.coalesce(referrerStep.getWorkflow_step_option(), new ArrayList());
				for (int i = 0; i < optionsForDelete.size(); i++) {
					Workflow_Step_Option optionForDelete = optionsForDelete.get(i);
					for (int j = 0; j < referrerOptions.size(); j++) {
						Workflow_Step_Option referrerOption = referrerOptions.get(j);
						if (Utils.stringAreEquals(optionForDelete.getValue(), referrerOption.getName())) {
							int timesValueIsReturned = workflowDao().timesWorkflowReturnValue(step.getId_workflow(), optionForDelete.getValue());
							if (timesValueIsReturned == 1) {
								workflow_Step_OptionDao().delete(referrerOption.getId());
								log().insert(user.getId(), "Opciones de pasos de flujos", referrerOption.getId(), "Eliminar", referrerOption, null, user.getIp());
							}
							referrerOptions.remove(j);
							break;
						}
					}
				}
			}
		}
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @param replaceInitial
	 * @return
	 */
	public String delete(final int id, User user, boolean replaceInitial) {
		int result;
		Workflow_Step before = get(id);
		if (!user.isMaster() && user.getCenterId() != before.getId_center()) {
			throw new WebApplicationException(String.format(FORBIDDEN, before.getId()), HttpStatus.FORBIDDEN_403);
		}
		List<Workflow_Step> dependentSteps = getDependentSteps(before.getId(), user);
		if (!dependentSteps.isEmpty()) {
			result = -1;
		} else if (!replaceInitial && before.isInitial_step()) {
			result = -2;
		} else {
			deleteOptions(before, user);
			result = dao().delete(id);
		}
		switch (result) {
			case 1:
				log().insert(user.getId(), "Pasos de flujos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, before.getName()), HttpStatus.PRECONDITION_FAILED_412);
			case -2:
				throw new WebApplicationException(String.format(CANT_DELETE_SYSTEM_ITEM, id), Response.Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @param id_workflow_version
	 * @param user
	 * @return
	 */
	public List<Workflow_Step> workflowDependencies(int id_workflow_version, User user) {
		WhereClause where = new WhereClause(
				new Where("ws.id_workflow_version", id_workflow_version),
				new Where("ws.id_type", WorkflowStepTypes.WORKFLOW.getValue())
		);
		List<Workflow_Step> steps = getAll(where);
		if (Utils.isNullOrEmpty(steps)) {
			return steps;
		}
		HashMap<Integer, Workflow> mapWorkflows = new HashMap();
		List<Integer> workflowIDs = steps.stream()
				.map(item -> item.getId_workflow_external())
				.collect(Collectors.toList());
		WhereClause whereW = new WhereClause(new Where("id", Where.IN, workflowIDs));
		List<Workflow> workflows = workflowService.getAll(whereW);
		workflows.forEach(workflow -> mapWorkflows.put(workflow.getId(), workflow));
		steps.forEach(step-> step.setWorkflow(mapWorkflows.get(step.getId_workflow_external())));
		return steps;
	}

	/**
	 *
	 * @param id_step
	 * @param user
	 * @return
	 */
	@Transaction
	public Workflow_Step markAsInitial(int id_step, User user) {
		Workflow_Step item = get(id_step);
		if (item.getVersion_active() == WorkflowVersionTypes.INACTIVE) {
			List<Workflow_Step> steps = getAll(item.getId_workflow_version());
			List<Workflow_Step> initialSteps = steps.stream()
					.filter(step -> step.isInitial_step())
					.collect(Collectors.toList());
			for (int i = 0; i < initialSteps.size(); i++) {
				Workflow_Step step = initialSteps.get(i);
				step.setInitial_step(false);
				dao().update(step);
			}

			item.setInitial_step(true);
			dao().update(item);

			Workflow_Step updated = get(id_step, true);
			return updated;
		}
		throw new WebApplicationException(CANT_UPDATE_INACTIVE_VERSION, Response.Status.NOT_MODIFIED);
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
