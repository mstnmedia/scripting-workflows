/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Content;
import com.mstn.scripting.workflows.db.ContentDao;
import com.mstn.scripting.core.models.Content_Language;
import com.mstn.scripting.workflows.db.Content_LanguageDao;
import com.mstn.scripting.workflows.db.Workflow_Step_ContentDao;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con las relaciones pasos-contenidos.
 *
 * @author josesuero
 */
public abstract class WorkflowStepContentService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract Workflow_Step_ContentDao dao();

	@CreateSqlObject
	abstract ContentDao contentDao();

	@CreateSqlObject
	abstract Content_LanguageDao contenLanguageDao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Workflow_Step_Content item, Workflow_Step_Content before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Workflow_Step_Content>> getRepresentation(WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Workflow_Step_Content> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(HttpStatus.OK_200, items, pageSize, pageNumber, totalRows);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow_Step_Content> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow_Step_Content> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Step_Content get(int id) {
		Workflow_Step_Content item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id_step
	 * @return
	 */
	public List<Workflow_Step_Content> getAllContent(int id_step) {
		List<Workflow_Step_Content> list = dao().getAll(id_step);
		for (int i = 0; i < list.size(); i++) {
			Workflow_Step_Content item = list.get(i);
			Content content = contentDao().get(item.getId());
			list.get(i).setContent(content);
		}
		return list;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public Workflow_Step_Content create(Workflow_Step_Content item, User user) {
		validateItemCenter(item, null, user);
		int id_content = item.getId_content();
		Content content = item.getContent();
		if (content != null && content.getId() == 0) {
			id_content = contentDao().insert(content);
			List<Content_Language> contentLanguage = content.getContent_language();
			for (int i = 0; i < contentLanguage.size(); i++) {
				Content_Language languageItem = contentLanguage.get(i);
				languageItem.setId_content(id_content);
				contenLanguageDao().insert(languageItem);
			}
		}
		item.setId_content(id_content);
		int id = dao().insert(item);

		Workflow_Step_Content after = get(id);
		log().insert(user.getId(), "Contenidos de pasos de flujos", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Workflow_Step_Content update(Workflow_Step_Content item, User user) {
		Workflow_Step_Content before = get(item.getId());
		validateItemCenter(item, before, user);
		dao().update(item);
		Workflow_Step_Content after = get(item.getId());
		log().insert(user.getId(), "Contenidos de pasos de flujos", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Workflow_Step_Content before = get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Contenidos de pasos de flujos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
