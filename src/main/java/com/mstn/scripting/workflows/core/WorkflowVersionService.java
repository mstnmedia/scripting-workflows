/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.workflows.ScriptingWorkflowsConfiguration;
import com.mstn.scripting.workflows.db.Workflow_Version;
import com.mstn.scripting.workflows.db.Workflow_VersionDao;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.apache.http.client.HttpClient;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las versiones de los flujos.
 *
 * @author josesuero
 */
public abstract class WorkflowVersionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";

	private ScriptingWorkflowsConfiguration config;
	private HttpClient httpClient;

	/**
	 *
	 */
	public WorkflowVersionService() {
	}

	/**
	 *
	 * @return
	 */
	public ScriptingWorkflowsConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public WorkflowVersionService setConfig(ScriptingWorkflowsConfiguration config) {
		this.config = config;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public WorkflowVersionService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	@CreateSqlObject
	abstract Workflow_VersionDao dao();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow_Version> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_workflow
	 * @return
	 */
	public List<Workflow_Version> getAll(int id_workflow) {
		return dao().getAll(id_workflow);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Version get(int id) {
		Workflow_Version item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
