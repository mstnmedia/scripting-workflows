package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Criteria_Condition;
import com.mstn.scripting.workflows.db.CriteriaConditionDao;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las condiciones de los criterios.
 *
 * @author MSTN Media
 */
public abstract class CriteriaConditionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract CriteriaConditionDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Criteria_Condition item, Criteria_Condition before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Criteria_Condition> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_criteria
	 * @return
	 */
	public List<Criteria_Condition> getAll(int id_criteria) {
		return dao().getAll(id_criteria);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Criteria_Condition get(int id) {
		Criteria_Condition item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Criteria_Condition create(Criteria_Condition item, User user) {
		validateItemCenter(item, null, user);
		int id = dao().insert(item);
		Criteria_Condition after = get(id);
		log().insert(user.getId(), "Condiciones de criterios", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Criteria_Condition update(Criteria_Condition item, User user) {
		Criteria_Condition before = get(item.getId());
		validateItemCenter(item, before, user);
		dao().update(item);
		Criteria_Condition after = get(item.getId());
		log().insert(user.getId(), "Condiciones de criterios", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Criteria_Condition before = get(id);
		int result = id == -1 ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Condiciones de criterios", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
