/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Criteria;
import com.mstn.scripting.core.models.Workflow_Criteria;
import com.mstn.scripting.workflows.db.Workflow_CriteriaDao;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las relaciones flujos-criterios.
 *
 * @author josesuero
 */
public abstract class WorkflowCriteriaService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	private CriteriaService criteriaService;

	/**
	 *
	 * @param criteriaService
	 * @return
	 */
	public WorkflowCriteriaService setCriteriaService(CriteriaService criteriaService) {
		this.criteriaService = criteriaService;
		return this;
	}

	@CreateSqlObject
	abstract Workflow_CriteriaDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Workflow_Criteria item, Workflow_Criteria before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow_Criteria> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_workflow
	 * @return
	 */
	public List<Workflow_Criteria> getAll(int id_workflow) {
		return dao().getAll(id_workflow);
	}

	/**
	 *
	 * @param id_workflow
	 * @param loadCriteria
	 * @return
	 */
	public List<Workflow_Criteria> getAll(int id_workflow, boolean loadCriteria) {
		List<Workflow_Criteria> list = getAll(id_workflow);
		setCriterias(list);
		return list;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow_Criteria get(int id) {
		Workflow_Criteria item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadCriteria
	 * @return
	 */
	public Workflow_Criteria get(int id, boolean loadCriteria) {
		Workflow_Criteria item = get(id);
		if (loadCriteria) {
			setCriteria(item);
		}
		return item;
	}

	void setCriteria(Workflow_Criteria wc) {
		wc.setCriteria(criteriaService.get(wc.getId_criteria()));
	}

	void setCriterias(List<Workflow_Criteria> WCs) {
		if (Utils.isNullOrEmpty(WCs)) {
			return;
		}
		List<Integer> criteriaIDs = WCs.stream()
				.map(item -> item.getId_criteria())
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id", Where.IN, criteriaIDs));
		List<Criteria> criterias = criteriaService.getAll(where);
		HashMap<Integer, Criteria> mapCriterias = new HashMap();
		criterias.forEach(c -> mapCriterias.put(c.getId(), c));
		WCs.forEach(wc -> wc.setCriteria(mapCriterias.get(wc.getId())));
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Workflow_Criteria create(Workflow_Criteria item, User user) {
		validateItemCenter(item, null, user);
		int id = dao().insert(item);
		Workflow_Criteria after = get(id);
		log().insert(user.getId(), "Criterios de flujos", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Workflow_Criteria update(Workflow_Criteria item, User user) {
		Workflow_Criteria before = get(item.getId());
		validateItemCenter(item, before, user);
		dao().update(item);
		Workflow_Criteria after = get(item.getId());
		log().insert(user.getId(), "Criterios de flujos", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Workflow_Criteria before = get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Criterios de flujos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
