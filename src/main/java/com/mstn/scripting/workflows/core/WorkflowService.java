/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import com.mstn.scripting.core.enums.WorkflowStepTypes;
import com.mstn.scripting.core.enums.WorkflowVersionTypes;
import com.mstn.scripting.core.models.Center;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.workflows.resources.WorkflowPublishParams;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.core.models.Workflow_Criteria;
import com.mstn.scripting.workflows.db.WorkflowDao;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.workflows.db.Workflow_StepDao;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import com.mstn.scripting.workflows.db.Workflow_Step_ContentDao;
import com.mstn.scripting.core.models.Workflow_Step_Interface_Field;
import com.mstn.scripting.workflows.db.Workflow_Step_Interface_FieldDao;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import com.mstn.scripting.workflows.ScriptingWorkflowsConfiguration;
import com.mstn.scripting.workflows.db.Workflow_Step_OptionDao;
import com.mstn.scripting.workflows.db.Workflow_Version;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import com.mstn.scripting.workflows.db.Workflow_VersionDao;
import com.mstn.scripting.workflows.resources.WorkflowPublishResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import org.apache.http.client.HttpClient;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con flujos.
 *
 * @author josesuero
 */
public abstract class WorkflowService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String PUBLISHING_ERROR = "Un error interno ha ocurrido mientras se publicaba esta versión.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DUPLICATED = "item id %s already exists.";
	private static final String NOT_CHANGED = "item id %s haven't changes.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	private WorkflowStepService workflowStepService;

	/**
	 *
	 * @param workflowStepService
	 * @return
	 */
	public WorkflowService setWorkflowStepService(WorkflowStepService workflowStepService) {
		this.workflowStepService = workflowStepService;
		this.workflowStepService.setWorkflowService(this);
		return this;
	}

	private WorkflowCriteriaService workflowCriteriaService;

	/**
	 *
	 * @param workflowCriteriaService
	 * @return
	 */
	public WorkflowService setWorkflowCriteriaService(WorkflowCriteriaService workflowCriteriaService) {
		this.workflowCriteriaService = workflowCriteriaService;
		return this;
	}

	private HttpClient httpClient;

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public WorkflowService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}
	private ScriptingWorkflowsConfiguration config;

	/**
	 *
	 * @return
	 */
	public ScriptingWorkflowsConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public WorkflowService setConfig(ScriptingWorkflowsConfiguration config) {
		this.config = config;
		return this;
	}

	@CreateSqlObject
	abstract WorkflowDao dao();

	@CreateSqlObject
	abstract LogDao log();

	@CreateSqlObject
	abstract Workflow_VersionDao workflowVersionDao();

	@CreateSqlObject
	abstract Workflow_StepDao workflowStepDao();

	@CreateSqlObject
	abstract Workflow_Step_OptionDao workflowStepOptionDao();

	@CreateSqlObject
	abstract Workflow_Step_ContentDao workflowStepContentDao();

	@CreateSqlObject
	abstract Workflow_Step_Interface_FieldDao workflowStepInterfaceFieldDao();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Workflow item, Workflow before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Workflow>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Workflow> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Workflow> getAllWithCriterias(WhereClause where) {
		List<Workflow> list = getAll(where);
		setCriterias(list, true);
		return list;
	}

	private Workflow get(int id) {
		Workflow item = dao().get(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 */
	public Workflow get(int id, boolean loadChildren) {
		Workflow workflow = get(id);
		if (loadChildren) {
			setSteps(workflow);
			setCriterias(workflow, true);
		}
		return workflow;
	}

	/**
	 *
	 * @param id
	 * @param version_id
	 * @param loadChildren
	 * @return
	 */
	public Workflow get(int id, int version_id, boolean loadChildren) {
		Workflow workflow = dao().get(id, version_id);
		if (workflow == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		if (loadChildren) {
			setSteps(workflow);
			setCriterias(workflow, true);
		}
		return workflow;
	}

	/**
	 *
	 * @param id
	 * @param version_id_version
	 * @return
	 */
	public Workflow getWithVersion(int id, String version_id_version) {
		Workflow workflow = dao().getWithVersion(id, version_id_version);
		setSteps(workflow);
		setCriterias(workflow, false);

		return workflow;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Workflow getActive(int id) {
		Workflow item = dao().getActiveVersion(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 */
	public Workflow getActive(int id, boolean loadChildren) {
		Workflow workflow = getActive(id);
		if (loadChildren) {
			setSteps(workflow);
			setCriterias(workflow, false);
		}
		return workflow;
	}

	void setCriterias(Workflow workflow, boolean load) {
		workflow.setWorkflow_criteria(workflowCriteriaService.getAll(workflow.getId(), load));
	}

	void setCriterias(List<Workflow> workflows, boolean load) {
		if (Utils.isNullOrEmpty(workflows)) {
			return;
		}
		HashMap<Integer, Workflow> mapWorkflows = new HashMap();
		List<Integer> workflowIDs = workflows.stream()
				.map(item -> {
					item.setWorkflow_criteria(new ArrayList());
					mapWorkflows.put(item.getId(), item);
					return item.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_workflow", Where.IN, workflowIDs));
		List<Workflow_Criteria> WCs = workflowCriteriaService.getAll(where);
		WCs.forEach(wc -> {
			Workflow workflow = mapWorkflows.get(wc.getId_workflow());
			workflow.getWorkflow_criteria().add(wc);
		});
		if (load) {
			workflowCriteriaService.setCriterias(WCs);
		}
	}

	void setSteps(Workflow workflow) {
		workflow.setWorkflow_step(workflowStepService.getAll(workflow.getVersion_id()));
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Workflow create(Workflow item, User user) throws Exception {
		validateItemCenter(item, null, user);
		int id = dao().insert(item);
		int id_version = workflowVersionDao().insert(
				new Workflow_Version(
						0, item.getId_center(), id, WorkflowVersionTypes.EDITABLE_ID_VERSION,
						new Date(), WorkflowVersionTypes.INACTIVE, "Versión editable", "", ""
				)
		);
		item.setId(id);
		item.setVersion_id(id_version);
		workflowStepService.createNoTransaction(new Workflow_Step(
				0, item.getId_center(), id, id_version,
				null, WorkflowStepTypes.UNDEFINED.getValue(), "Paso inicial",
				0, 0, 0, true
		), workflowStepDao(), workflowStepContentDao(), user);
		Workflow after = get(id);
		log().insert(user.getId(), "Flujos", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public Workflow update(Workflow item, User user) {
		Workflow before = get(item.getId());
		validateItemCenter(item, before, user);
		dao().update(item);
		Workflow after = get(item.getId());
		log().insert(user.getId(), "Flujos", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Workflow before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Flujos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @param config
	 * @param user
	 * @return
	 */
	@Transaction
	public WorkflowPublishResponse publish(final WorkflowPublishParams config, User user) {
		// Se podrían hacer las validaciones por separado desde el cliente web para ir avisando al usuario
		// el avance del paso a paso de la validación
		// HowTODO: Debe validar que el flujo teanga cambios en la versión editable (dar uso a propiedad Workflow.has_changes)
		// HowTODO: Notificar cuando un subflujo tiene cambios pendientes de publicar
		// EvaluateTODO: Validar que al publicar un secundario todos los primarios que lo invocan usan todos los valores que devuelve el secundario y lo usan
		// TODO: Debe validar que todas las interfaces devuelven valor que se pueda mapear con las opciones correspondientes
		// TODO: Crear constraint o índice que prohiba más de una versión activa por flujo
		// TODO: Notificar cuando un paso del flujo, excepto el inicial, no es referenciado por otro paso del mismo flujo.
		// TODO: Validar cuando una opción de un paso cambia de tipo, para eliminar o crear las opciones en los pasos referenciantes

		// Debe validar que el flujo tiene un fin en todos los caminos y que no devuelven valores.
		// Debe validar que todos los flujos secundarios devuelven valor que se pueda mapear con las opciones correspondientes
		// Validar que todos los subflujos tienen una versión publicada
		// Validar que los flujos tienen sólo un inicio y es el paso de indice menor(para obtener el primero de un subflujo)
		WorkflowPublishResponse response = new WorkflowPublishResponse();
		response.config = config;

		Workflow workflow = get(response.config.getId_workflow(), response.config.getId_workflow_version(), false);
		response.workflow = workflow;

		List<Workflow_Version> versions = workflowVersionDao().getAll(response.workflow.getId());
		validatePublishVersionName(response, versions);
		validatePublishWorkflow(response);

		OptionalInt max = response.notes.stream().mapToInt(i -> i.getId()).max();
		if (PublishResponseCode.ERROR > max.orElse(PublishResponseCode.SUCCESS)) {
			try {
				versions.forEach(version -> {
					if (version.getActive() != WorkflowVersionTypes.INACTIVE) {
						if (version.getId() == response.config.getId_workflow_version()) {
							version.setActive(WorkflowVersionTypes.ACTIVE);
						} else if (version.getActive() == WorkflowVersionTypes.ACTIVE) {
							version.setActive(WorkflowVersionTypes.ARCHIVED);
						}
						workflowVersionDao().update(version);
					}
				});
				if (response.workflow.getVersion_active() == WorkflowVersionTypes.ARCHIVED) {
					response.addNote(PublishResponseCode.SUCCESS, "La versión " + config.getVersion()
							+ " del flujo " + workflowName(response.workflow)
							+ " ha sido activada.");
					return response;
				}

				int idNewVersion = workflowVersionDao().insert(
						new Workflow_Version(
								0, response.workflow.getId_center(), response.workflow.getId(), config.getVersion(),
								new Date(), WorkflowVersionTypes.ACTIVE, config.getNotes(), "", ""
						)
				);
				response.id_version = idNewVersion;

				List<Workflow_Step_Option> options = new ArrayList<>();
				for (Workflow_Step step : response.steps) {
					step.setId_workflow_version(response.id_version);
					int newStepId = workflowStepDao().insert(step);
					step.setId(newStepId);

					for (Workflow_Step_Option option : step.getWorkflow_step_option()) {
						option.setId_workflow_step(step.getId());
						option.setId_workflow_version(response.id_version);
						options.add(option);
					}

					for (Workflow_Step_Content stepContent : step.getWorkflow_step_content()) {
						workflowStepContentDao().insert(stepContent, step);
					}

					for (Workflow_Step_Interface_Field wsif : step.getWorkflow_step_interface_field()) {
						wsif.setId_workflow_step(step.getId());
						workflowStepInterfaceFieldDao().insert(wsif);
					}
				}
				options.forEach((option) -> {
					workflowStepOptionDao().insert(option);
				});
				response.addNote(PublishResponseCode.SUCCESS, "La versión " + config.getVersion()
						+ " del flujo " + workflowName(response.workflow)
						+ " ha sido publicada.");
				log().insert(user.getId(), "Flujos", response.workflow.getId(), "Publicar", config, null, user.getIp());
			} catch (Exception ex) {
				log().insert(user.getId(), "Flujos", response.workflow.getId(), "Error publicando", ex.getMessage() + "\n" + JSON.toString(config), ex, user.getIp());
				response.addNote(PublishResponseCode.ERROR, PUBLISHING_ERROR);
				Utils.logException(WorkflowService.class, ex.getMessage(), ex);
			}
		}
		response.id_workflow = config.getId_workflow();
		response.result = response.notes.stream().mapToInt(i -> i.getId()).max().orElse(PublishResponseCode.SUCCESS);
		return response;
	}

	class PublishResponseCode {

		static public final int SUCCESS = 1;
		static public final int WARN = 2;
		static public final int ERROR = 3;
	}

	String versionError(WorkflowPublishResponse payload) {
		return "El flujo " + workflowName(payload.workflow) + " ya tiene una versión '" + payload.config.getVersion() + "'";
	}

	String workflowName(Workflow item) {
		return item.getId() + " | '" + item.getName() + "'";
	}

	String stepName(Workflow_Step item) {
		return item.getId() + " | '" + item.getName() + "'";
	}

	String optionName(Workflow_Step_Option item) {
		return item.getId() + " | '" + item.getName() + "'";
	}

	WorkflowPublishResponse validatePublishWorkflow(WorkflowPublishResponse response) {
		try {
			response.referers = workflowStepService.getReferersOfSubWorkflow(response.workflow.getId());

			List<Workflow_Step> steps = workflowStepService.getAll(response.workflow.getVersion_id(), true, false);
			steps.forEach(step -> response.mapSteps.put(step.getId(), step));
			response.steps = steps;

			List<Integer> subWorkflowsIDs = steps
					.stream()
					.mapToInt(
							step -> step.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()
							? step.getId_workflow_external()
							: 0)
					.boxed()
					.filter(i -> i != 0)
					.collect(Collectors.toList());
			if (!subWorkflowsIDs.isEmpty()) {
				WhereClause where = new WhereClause(new Where("id", Where.IN, subWorkflowsIDs));
				response.subWorkflows = getAll(where, Integer.MAX_VALUE, 1, "ID");
				validatePublishSubWorkflowVersions(response);
				for (int i = 0; i < response.subWorkflows.size(); i++) {
					Workflow subWorkflow = response.subWorkflows.get(i);
					response.mapSubWorkflows.put(subWorkflow.getId(), subWorkflow);
					response.mapSubWorkflowReturnedValues.put(subWorkflow.getId(), workflowStepService.getValueOptionsOfSubWorkflow(subWorkflow.getVersion_id()));
				}
			}
			validatePublishSteps(response);
		} catch (Exception ex) {
			Utils.logException(WorkflowService.class, ex.getMessage(), ex);
			response.addNote(PublishResponseCode.ERROR, PUBLISHING_ERROR);
		}
		return response;
	}

	void validatePublishVersionName(WorkflowPublishResponse response, List<Workflow_Version> versions) {
		for (int i = 0; i < versions.size(); i++) {
			Workflow_Version version = versions.get(i);
			if (Utils.stringAreEquals(version.getId_version(), response.config.getVersion())
					&& (version.getId() != response.config.getId_workflow_version()
					|| version.getActive() == WorkflowVersionTypes.INACTIVE)) {
//				versions.clear();
				response.addNote(PublishResponseCode.ERROR, versionError(response));
				break;
			}
		}
	}

	void validatePublishSubWorkflowVersions(WorkflowPublishResponse payload) {
		for (int i = 0; i < payload.subWorkflows.size(); i++) {
			Workflow subWorkflow = payload.subWorkflows.get(i);
			if (subWorkflow.getVersion_id() == 0) {
				payload.addNote(PublishResponseCode.ERROR, "El sub-flujo " + workflowName(subWorkflow) + " no tiene una versión activa. Debe publicarlo antes de continuar.");
			}
		}
	}

	void validatePublishSteps(WorkflowPublishResponse payload) {
		int initialStepID = 0;
		for (int i = 0; i < payload.steps.size(); i++) {
			Workflow_Step step = payload.steps.get(i);
			int stepType = step.getId_type();
			if (stepType == 0 || stepType == WorkflowStepTypes.UNDEFINED.getValue()) {
				payload.addNote(PublishResponseCode.ERROR, "El paso " + stepName(step) + " no tiene un tipo definido.");
			} else if (stepType == WorkflowStepTypes.REGULAR.getValue()) {
				if (Utils.listIsNullOrEmpty(step.getWorkflow_step_content())) {
					payload.addNote(PublishResponseCode.WARN, "El paso " + stepName(step) + " no tiene contenidos.");
				}
			} else if (step.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
				if (step.getId_workflow_external() == 0) {
					payload.addNote(PublishResponseCode.ERROR, "El paso " + stepName(step) + " no hace referencia a ningún flujo.");
				}
			} else if (step.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
				if (step.getId_workflow_external() == 0) {
					payload.addNote(PublishResponseCode.ERROR, "El paso " + stepName(step) + " no hace referencia a ninguna interfaz.");
				}
			}
			validatePublishOptions(step, payload);
			if (step.isInitial_step()) {
				if (initialStepID == 0) {
					initialStepID = step.getId();
				} else {
					payload.addNote(PublishResponseCode.ERROR, "El flujo " + workflowName(payload.workflow) + " tiene multiples pasos iniciales.");
				}
			}
		}
		if (initialStepID == 0) {
			payload.addNote(PublishResponseCode.ERROR, "El flujo " + workflowName(payload.workflow) + " no tiene paso inicial.");
		}
	}

	void validatePublishOptions(Workflow_Step step, WorkflowPublishResponse payload) {
		// Validating step has options to continue or end
		if (Utils.listIsNullOrEmpty(step.getWorkflow_step_option())) {
			payload.addNote(PublishResponseCode.ERROR, "El paso " + stepName(step) + " no tiene opciones.");
			return;
		}
		// Validating option configurations of this step
		for (int i = 0; i < step.getWorkflow_step_option().size(); i++) {
			Workflow_Step_Option option = step.getWorkflow_step_option().get(i);
			int optionType = option.getId_type();
			if (optionType == WorkflowStepOptionTypes.UNDEFINED) {
				payload.addNote(PublishResponseCode.ERROR, "La opción " + optionName(option) + " del paso " + stepName(step) + " no tiene un tipo definido.");
			} else if (optionType == WorkflowStepOptionTypes.STEP) {
				if (Utils.stringIsNullOrEmpty(option.getValue())) {
					payload.addNote(PublishResponseCode.ERROR, "La opción " + optionName(option) + " del paso " + stepName(step) + " no hace referencia a un paso.");
				} else if (null == payload.mapSteps.getOrDefault(Integer.parseInt(option.getValue()), null)) {
					payload.addNote(PublishResponseCode.ERROR, "La opción " + optionName(option) + " del paso " + stepName(step) + " hace referencia a un paso inexistente.");
				}
			} else if (optionType == WorkflowStepOptionTypes.VALUE && Utils.stringIsNullOrEmpty(option.getValue())) {
				payload.addNote(PublishResponseCode.WARN, "La opción " + optionName(option) + " del paso " + stepName(step) + " no tiene un valor asignado.");
			}
		}

		if (step.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
			// Validating step options are updated from subworkflow referenced
			Workflow subWorkflow = payload.mapSubWorkflows.get(step.getId_workflow_external());
			if (subWorkflow == null) {
				payload.addNote(PublishResponseCode.ERROR, "El paso " + stepName(step) + " hace referencia a un flujo inexistente.");
			} else {
				// Obteniendo valores retornados, ignorando duplicados
				HashMap<String, Workflow_Step_Option> map = new HashMap();
				List<Workflow_Step_Option> publishedOptions = payload.mapSubWorkflowReturnedValues.getOrDefault(subWorkflow.getId(), new ArrayList());
				for (int i = 0; i < publishedOptions.size(); i++) {
					Workflow_Step_Option option = publishedOptions.get(i);
					map.put(option.getValue(), option);
				}
				publishedOptions = new ArrayList(map.values());

				for (int i = 0; i < step.getWorkflow_step_option().size(); i++) {
					Workflow_Step_Option option = step.getWorkflow_step_option().get(i);
					for (int j = 0; j < publishedOptions.size(); j++) {
						Workflow_Step_Option pOption = publishedOptions.get(j);
						if (Utils.stringAreEquals(pOption.getValue(), option.getName())) {
							publishedOptions.remove(j);
							break;
						}
					}
				}
				for (int j = 0; j < publishedOptions.size(); j++) {
					Workflow_Step_Option pOption = publishedOptions.get(j);
					payload.addNote(PublishResponseCode.ERROR, "El valor retornado'" + pOption.getValue() + "'"
							+ " del sub-flujo " + workflowName(subWorkflow)
							+ " no está presente en las opciones del paso " + stepName(step));
				}
			}
		} else if (step.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
			// TODO: Validar que utiliza todos los valores qe retorna la interfaz.
		}

	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public Workflow export(int id, User user) {
		try {
			Workflow workflow = get(id);
			try {
				List<Workflow_Criteria> criterias = workflowCriteriaService.getAll(workflow.getId(), true);
				workflow.setWorkflow_criteria(criterias);
			} catch (Exception ex) {
				throw new WebApplicationException("No se pudo consultar los criterios de este flujo", ex);
			}
			List<Workflow_Step> steps = Arrays.asList();
			try {
				steps = workflowStepService.getAll(workflow.getVersion_id());
			} catch (Exception ex) {
				throw new WebApplicationException("No se pudo consultar los pasos de este flujo", ex);
			}
			List<Interface_Service> services = Arrays.asList();
			HashMap<Integer, Interface_Service> servicesMap = new HashMap();
			try {
				List<Integer> interfaceServiceIDs = steps.stream()
						.filter(i -> i.getId_type() == WorkflowStepTypes.EXTERNAL.getValue())
						.map(i -> i.getId_interface_service())
						.distinct()
						.collect(Collectors.toList());
				if (!interfaceServiceIDs.isEmpty()) {
					List<Where> filter = Arrays.asList(new Where("id", Where.IN, interfaceServiceIDs));
					services = Interface_Service.getInterfaceServices(JSON.toString(filter), true, httpClient, config.getApiURL(), user.getToken());
					services.forEach(i -> servicesMap.put(i.getId(), i));
				}
			} catch (Exception ex) {
				throw new WebApplicationException("No se pudo consultar los servicios de las interfaces", ex);
			}
			for (int i = 0; i < steps.size(); i++) {
				Workflow_Step step = steps.get(i);
				step.setWorkflow_step_option(workflowStepService.getAllOption(step.getId(), false));
				if (step.getId_type() == WorkflowStepTypes.WORKFLOW.getValue()) {
					// TODO: What load if step is typed as workflow
				}
				if (step.getId_type() != WorkflowStepTypes.WORKFLOW.getValue()) {
					step.setWorkflow_step_content(workflowStepService.getAllContent(step.getId(), true));
				}
				if (step.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
					step.setWorkflow_step_interface_field(workflowStepService.getAllFields(step.getId()));
					Interface_Service service = servicesMap.getOrDefault(step.getId_interface_service(), new Interface_Service());
					Interface_Service cloned = JSON.clone(service);
					Interface inter = cloned.getInterface();
					cloned.setInterface(null);
					if (inter == null) {
						throw new WebApplicationException(String.format("No se encontró la interfaz %s para el paso %s.", step.getId_workflow_external(), stepName(step)));
					}
					inter.setInterface_services(new ArrayList());
					inter.getInterface_services().add(cloned);
					step.setInterface(inter);
				}
			}
			workflow.setWorkflow_step(steps);
			return workflow;
		} catch (Exception ex) {
//			log().insert(user.getId(), "Flujos", id, "Error exportando", ex.getMessage(),ex, user.getIp());
			throw new WebApplicationException("Ha ocurrido un error exportando el flujo: "
					+ ex.getMessage()
					+ ". Consulte los registros de logs para más información.",
					ex);
		}
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public WorkflowPublishResponse importWorkflow(Workflow item, User user) {
		WorkflowPublishResponse response = new WorkflowPublishResponse();
		response.workflow = item;
		validateImportWorkflow(response, user);

		OptionalInt max = response.notes.stream().mapToInt(i -> i.getId()).max();
		if (PublishResponseCode.ERROR > max.orElse(PublishResponseCode.SUCCESS)) {
			List<Workflow_Step> steps = item.getWorkflow_step();
			long hasInitial = steps.stream()
					.filter(i -> i.isInitial_step())
					.count();
			if (hasInitial == 0) {
				response.addNote(PublishResponseCode.ERROR, "El flujo no tiene paso inicial.");
			} else if (hasInitial > 1) {
				response.addNote(PublishResponseCode.ERROR, "Tiene varios pasos iniciales.");
			} else {
				for (int i = 0; i < steps.size(); i++) {
					Workflow_Step step = steps.get(i);
//					if(step.)
				}
			}
		}
		return response;
	}

	void validateImportWorkflow(WorkflowPublishResponse response, User user) {
		Workflow item = response.workflow;
		if (Utils.stringIsNullOrEmpty(item.getName())) {
			response.addNote(PublishResponseCode.ERROR, "El flujo debe tener un nombre.");
		}
		if (item.getMain() != 0 && item.getMain() != 1) {
			response.addNote(PublishResponseCode.ERROR, "La propiedad 'main' del flujo debe ser 0 ó 1.");
		}
		if (Utils.listIsNullOrEmpty(item.getWorkflow_step())) {
			response.addNote(PublishResponseCode.ERROR, "Este flujo no tiene pasos.");
		}
		if (Utils.listIsNullOrEmpty(item.getWorkflow_criteria())) {
			response.addNote(PublishResponseCode.WARN, "Este flujo no tiene criterios.");
		}
		try {
			Center center = Center.getCenter(item.getId_center(), httpClient, config.getApiURL(), user.getToken());
			item.setCenter_name(center.getName());
		} catch (Exception ex) {
			response.addNote(PublishResponseCode.ERROR, "No se pudo consultar el centro " + item.getId_center() + ".");
			Utils.logException(this.getClass(), "Error al consultar centro " + item.getId_center() + " importando flujo.", ex);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
