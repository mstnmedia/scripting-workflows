/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.core;

import com.mstn.scripting.core.FileManager;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.ContentTypes;
import com.mstn.scripting.core.models.Content;
import com.mstn.scripting.workflows.ScriptingWorkflowsApplication;
import com.mstn.scripting.workflows.db.ContentDao;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con contenidos.
 *
 * @author josesuero
 */
public abstract class ContentService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract ContentDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("(", Where.OR,
				new Where("id_center", User.SYSTEM.getId()),
				new Where("id_center", user.getCenterId())
		);
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void validateItemCenter(Content item, Content before, User user) {
		if (!user.isMaster()) {
			if (before != null && before.getId_center() != item.getId_center()) {
				throw new WebApplicationException("No tiene permiso para cambiar el centro del registro.", HttpStatus.BAD_REQUEST_400);
			} else if (user.getCenterId() != 0) {
				item.setId_center(user.getCenterId());
			} else {
				throw new WebApplicationException("Este usuario no pertenece a ningún centro.", HttpStatus.BAD_REQUEST_400);
			}
		} else if (item.getId_center() == 0) {
			throw new WebApplicationException("Especifique un centro para este registro.", HttpStatus.BAD_REQUEST_400);
		}
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Content>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Content> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Content> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Content> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Content get(int id) {
		Content item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param itemID
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Content duplicate(int itemID, User user) throws Exception {
		Content item = get(itemID);
		item.setName(item.getName() + " - Copia");
		int id = dao().insert(item);
		Content after = get(id);
		log().insert(user.getId(), "Contenidos", id, "Duplicar", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Content create(Content item, User user) throws Exception {
		validateItemCenter(item, null, user);
		if (item.getId_type() == ContentTypes.IMAGE.getValue()
				|| item.getId_type() == ContentTypes.FILE.getValue()) {
			String uploadPath = ScriptingWorkflowsApplication.uploadFolder;
			String contentsPath = ScriptingWorkflowsApplication.contentsFolder;

			String fromPath = uploadPath + item.getValue();
			String toPath = contentsPath + item.getValue();
			boolean fileMoved = FileManager.moveFile(fromPath, toPath);
			if (!fileMoved) {
				throw new Exception("Ha ocurrido un error moviendo el archivo.");
			}
		}
		int id = dao().insert(item);
		Content after = get(id);
		log().insert(user.getId(), "Contenidos", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Content update(Content item, User user) throws Exception {
		Content before = get(item.getId());
		validateItemCenter(item, before, user);
		if ((item.getId_type() == ContentTypes.IMAGE.getValue()
				|| item.getId_type() == ContentTypes.FILE.getValue())
				&& !Utils.stringAreEquals(before.getValue(), item.getValue())) {
			String uploadPath = ScriptingWorkflowsApplication.uploadFolder;
			String contentsPath = ScriptingWorkflowsApplication.contentsFolder;

			String fromPath = uploadPath + item.getValue();
			String toPath = contentsPath + item.getValue();
			boolean fileMoved = FileManager.moveFile(fromPath, toPath);
			if (!fileMoved) {
				throw new Exception("Ha ocurrido un error moviendo el archivo.");
			}
		}
		dao().update(item);
		Content after = get(item.getId());
		log().insert(user.getId(), "Contenidos", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Content before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Contenidos", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Response.Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
