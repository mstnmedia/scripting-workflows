package com.mstn.scripting.workflows;

import com.mstn.scripting.workflows.core.ContentService;
import com.mstn.scripting.workflows.core.WorkflowService;
import com.mstn.scripting.workflows.core.WorkflowStepContentService;
import com.mstn.scripting.workflows.core.WorkflowStepOptionService;
import com.mstn.scripting.workflows.core.WorkflowStepService;
import com.mstn.scripting.workflows.health.ScriptingWorkflowsHealthCheck;
import com.mstn.scripting.workflows.resources.ContentResource;
import com.mstn.scripting.workflows.resources.WorkflowResource;
import com.mstn.scripting.workflows.resources.WorkflowStepContentResource;
import com.mstn.scripting.workflows.resources.WorkflowStepOptionResource;
import com.mstn.scripting.workflows.resources.WorkflowStepResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.PolymorphicAuthDynamicFeature;
import io.dropwizard.auth.PolymorphicAuthValueFactoryProvider;
import io.dropwizard.auth.PrincipalImpl;
import io.dropwizard.auth.basic.BasicCredentials;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtContext;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mstn.scripting.core.auth.AuthFilterUtils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.workflows.core.CriteriaConditionService;
import com.mstn.scripting.workflows.core.CriteriaService;
import com.mstn.scripting.workflows.core.WorkflowCriteriaService;
import com.mstn.scripting.workflows.core.WorkflowVersionService;
import com.mstn.scripting.workflows.resources.CriteriaResource;
import com.mstn.scripting.workflows.resources.WorkflowCriteriaResource;
import com.mstn.scripting.workflows.resources.WorkflowVersionResource;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.client.HttpClient;
import org.skife.jdbi.v2.DBI;

/**
 * Clase ejecutable que inicia el servidor y registra los componentes necesarios
 * para el funcionamiento del microservicio Workflows.
 *
 * @author MSTN Media
 */
public class ScriptingWorkflowsApplication extends Application<ScriptingWorkflowsConfiguration> {

	private static final String WORKFLOW_SERVICE = "Workflow service";

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		new ScriptingWorkflowsApplication().run(args);
	}

	/**
	 *
	 */
	static public String uploadFolder;

	/**
	 *
	 */
	static public String contentsFolder;

	@Override
	public String getName() {
		return "ScriptingWorkflows";
	}

	@Override
	public void initialize(final Bootstrap<ScriptingWorkflowsConfiguration> bootstrap) {
		bootstrap.addBundle(new MigrationsBundle<ScriptingWorkflowsConfiguration>() {
			@Override
			public PooledDataSourceFactory getDataSourceFactory(ScriptingWorkflowsConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}
		});
		bootstrap.addBundle(new DBIExceptionsBundle());
	}

	@Override
	public void run(final ScriptingWorkflowsConfiguration config, final Environment environment) {
		TimeZone.setDefault(TimeZone.getTimeZone(config.getTimeZone()));
		Locale.setDefault(Locale.forLanguageTag(config.getLocale()));
		uploadFolder = config.getUploadFolder();
		contentsFolder = config.getContentsFolder();

		registerResources(config, environment);
		registerAuthFilters(environment);
	}

	private void registerResources(final ScriptingWorkflowsConfiguration config, final Environment environment) {
		final DBIFactory factory = new DBIFactory();
		final DBI dbi = factory.build(environment, config.getDataSourceFactory(), "database");

		final HttpClient httpClient = new HttpClientBuilder(environment)
				.using(config.getHttpClientConfiguration())
				.build(getName());
		CriteriaConditionService conditionService = dbi.onDemand(CriteriaConditionService.class);
		CriteriaService criteriaService = dbi.onDemand(CriteriaService.class)
				.setConditionService(conditionService);
		WorkflowCriteriaService workflowCriteriaService = dbi.onDemand(WorkflowCriteriaService.class)
				.setCriteriaService(criteriaService);
		WorkflowStepService workflowStepService = dbi.onDemand(WorkflowStepService.class)
				.setConfig(config)
				.setHttpClient(httpClient);
		WorkflowService workflowService = dbi.onDemand(WorkflowService.class)
				.setWorkflowStepService(workflowStepService)
				.setWorkflowCriteriaService(workflowCriteriaService)
				.setConfig(config)
				.setHttpClient(httpClient);

		ScriptingWorkflowsHealthCheck healthCheck = new ScriptingWorkflowsHealthCheck(dbi.onDemand(WorkflowService.class));
		environment.healthChecks().register(WORKFLOW_SERVICE, healthCheck);
		environment.jersey().register(new ContentResource(dbi.onDemand(ContentService.class)));
		environment.jersey().register(new CriteriaResource(criteriaService));
		environment.jersey().register(new WorkflowResource(workflowService));
		environment.jersey().register(new WorkflowCriteriaResource(workflowCriteriaService));
		environment.jersey().register(new WorkflowStepResource(workflowStepService));
		environment.jersey().register(new WorkflowStepContentResource(dbi.onDemand(WorkflowStepContentService.class)));
		environment.jersey().register(new WorkflowStepOptionResource(
				workflowStepService,
				dbi.onDemand(WorkflowStepOptionService.class)
		));
		environment.jersey().register(new WorkflowVersionResource(dbi.onDemand(WorkflowVersionService.class)));
	}

	/**
	 * Registers the filters that will handle authentication
	 */
	private void registerAuthFilters(Environment environment) {
		AuthFilterUtils authFilterUtils = new AuthFilterUtils();
		final AuthFilter<BasicCredentials, PrincipalImpl> basicFilter = authFilterUtils.buildBasicAuthFilter();
		final AuthFilter<JwtContext, User> jwtFilter = authFilterUtils.buildJwtAuthFilter();

		final PolymorphicAuthDynamicFeature feature = new PolymorphicAuthDynamicFeature<>(
				ImmutableMap.of(PrincipalImpl.class, basicFilter, User.class, jwtFilter));
		final AbstractBinder binder = new PolymorphicAuthValueFactoryProvider.Binder<>(
				ImmutableSet.of(PrincipalImpl.class, User.class));

		environment.jersey().register(feature);
		environment.jersey().register(binder);
		environment.jersey().register(RolesAllowedDynamicFeature.class);
	}
}
