package com.mstn.scripting.workflows;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define la configuración del microservicio Workflows.
 *
 * @author MSTN Media
 */
public class ScriptingWorkflowsConfiguration extends Configuration {

	private static final String DATABASE = "database";
	private static final String API_URL = "apiURL";
	private static final String HTTP_CLIENT = "httpClient";
	private static final String TIME_ZONE = "timeZone";

	@NotEmpty
	private String uploadFolder;

	/**
	 *
	 * @return
	 */
	@JsonProperty("uploadFolder")
	public String getUploadFolder() {
		return uploadFolder;
	}

	/**
	 *
	 * @param uploadFolder
	 */
	@JsonProperty("uploadFolder")
	public void setUploadFolder(String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}

	@NotEmpty
	private String contentsFolder;

	/**
	 *
	 * @return
	 */
	@JsonProperty("contentsFolder")
	public String getContentsFolder() {
		return contentsFolder;
	}

	/**
	 *
	 * @param contentsFolder
	 */
	@JsonProperty("contentsFolder")
	public void setContentsFolder(String contentsFolder) {
		this.contentsFolder = contentsFolder;
	}

	@NotEmpty
	private String apiURL;

	@NotEmpty
	private String timeZone;

	@Valid
	@NotNull
	private DataSourceFactory dataSourceFactory = new DataSourceFactory();
	@Valid
	@NotNull
	private HttpClientConfiguration httpClient = new HttpClientConfiguration();

	/**
	 *
	 * @return
	 */
	@JsonProperty(DATABASE)
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	/**
	 *
	 * @param dataSourceFactory
	 */
	@JsonProperty(DATABASE)
	public void setDataSourceFactory(final DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(API_URL)
	public String getApiURL() {
		return apiURL;
	}

	/**
	 *
	 * @param apiURL
	 */
	@JsonProperty(API_URL)
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(HTTP_CLIENT)
	public HttpClientConfiguration getHttpClientConfiguration() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 */
	@JsonProperty(HTTP_CLIENT)
	public void setHttpClientConfiguration(HttpClientConfiguration httpClient) {
		this.httpClient = httpClient;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(TIME_ZONE)
	public String getTimeZone() {
		return timeZone;
	}

	@NotEmpty
	private String locale;

	/**
	 *
	 * @return
	 */
	@JsonProperty("locale")
	public String getLocale() {
		return locale;
	}
}
