/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code workflow}.
 *
 * @author josesuero
 */
@RegisterMapper(WorkflowDao.WorkflowMapper.class)
@UseStringTemplate3StatementLocator
public abstract class WorkflowDao {

	/**
	 *
	 */
	static public class WorkflowMapper implements ResultSetMapper<Workflow> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String CENTER_NAME = "center_name";
		private static final String NAME = "name";
		private static final String TAGS = "tags";
		private static final String MAIN = "main";
		private static final String HAS_CHANGES = "has_changes";
		private static final String DELETED = "deleted";
		private static final String VERSION_ID = "version_id";
		private static final String VERSION_ID_VERSION = "version_id_version";
		private static final String VERSION_DOCDATE = "version_docdate";
		private static final String VERSION_ACTIVE = "version_active";
		private static final String VERSION_NOTES = "version_notes";

		@Override
		public Workflow map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(TAGS) ? resultSet.getString(TAGS) : "",
					hasColumn.containsKey(MAIN) ? resultSet.getInt(MAIN) : 0,
					hasColumn.containsKey(HAS_CHANGES) ? resultSet.getBoolean(HAS_CHANGES) : Boolean.FALSE,
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : Boolean.FALSE,
					hasColumn.containsKey(VERSION_ID) ? resultSet.getInt(VERSION_ID) : 0,
					hasColumn.containsKey(VERSION_ID_VERSION) ? resultSet.getString(VERSION_ID_VERSION) : "",
					DATE.getTimestamp(hasColumn, resultSet, VERSION_DOCDATE),
					hasColumn.containsKey(VERSION_ACTIVE) ? resultSet.getInt(VERSION_ACTIVE) : 0,
					hasColumn.containsKey(VERSION_NOTES) ? resultSet.getString(VERSION_NOTES) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_workflow <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, w.* from ("
			+ "  select w.* from v_workflow w <where> order by <order_by>"
			+ " ) w"
			+ ") w where w.rowno >= ((:page_number - 1) * :page_size + 1) and w.rowno \\<= (:page_number * :page_size)")
	abstract public List<Workflow> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_workflow <where> order by ID desc")
	abstract public List<Workflow> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select w.*, c.name center_name, "
			+ " v.id version_id, v.id_version version_id_version, "
			+ " v.docdate version_docdate, v.active version_active, v.notes version_notes "
			+ "from WORKFLOW w "
			+ " join center c on c.id=w.id_center"
			+ "	left join WORKFLOW_VERSION v on v.ID_WORKFLOW = w.ID and v.Active = 0 "
			+ "where w.id = :id")
	public abstract Workflow get(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("SELECT w.*, c.name center_name, "
			+ " v.id version_id, v.id_version version_id_version, "
			+ " v.docdate version_docdate, v.active version_active, v.notes version_notes "
			+ "from WORKFLOW w "
			+ " join center c on c.id=w.id_center"
			+ "	join WORKFLOW_VERSION v on v.id_workflow = w.id and v.Active = 1 "
			+ "where w.id = :id order by v.docdate desc")
	public abstract Workflow getActiveVersion(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @param id_version
	 * @return
	 */
	@SqlQuery("SELECT w.*, c.name center_name, "
			+ " v.id version_id, v.id_version version_id_version,"
			+ " v.docdate version_docdate, v.active version_active, v.notes version_notes "
			+ "FROM workflow w "
			+ " join center c on c.id=w.id_center"
			+ " join workflow_version v on w.id = v.id_workflow "
			+ "where w.id = :id and v.id_version = :id_version")
	public abstract Workflow getWithVersion(@Bind("id") final int id, @Bind("id_version") final String id_version);

	/**
	 *
	 * @param id
	 * @param version_id
	 * @return
	 */
	@SqlQuery("SELECT w.*, c.name center_name,"
			+ " v.id version_id, v.id_version version_id_version,"
			+ " v.docdate version_docdate, v.active version_active, v.notes version_notes "
			+ "FROM workflow w "
			+ "	join center c on c.id=w.id_center"
			+ "	join workflow_version v on w.id = v.id_workflow "
			+ "where w.id=:id and v.id=:version_id")
	public abstract Workflow get(@Bind("id") final int id, @Bind("version_id") final int version_id);

	/**
	 *
	 * @param id_workflow
	 * @param value
	 * @return
	 */
	@SqlQuery("select count(wso.id) Times "
			+ "from workflow w "
			+ "join workflow_version wv on wv.id_workflow = w.id "
			+ "join workflow_step_option wso on wso.id_workflow_version = wv.id "
			+ "where w.id=:id_workflow and wv.active = 0"
			+ " and wso.id_type = " + WorkflowStepOptionTypes.VALUE
			+ " and wso.value=:value")
	public abstract int timesWorkflowReturnValue(@Bind("id_workflow") final int id_workflow, @Bind("value") final String value);

	/**
	 *
	 * @param workflow
	 * @return
	 */
	@SqlUpdate("insert into workflow(id, id_center, name, tags, main, has_changes, deleted)"
			+ " values(workflow_seq.nextVal, :id_center, :name, :tags, :main, :has_changes, :deleted)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	public abstract int insert(@BindBean final Workflow workflow);

	/**
	 *
	 * @param workflow
	 */
	@SqlUpdate("update workflow set id_center = :id_center,"
			+ " name = :name, tags = :tags,"
			+ " main = :main, has_changes = :has_changes,"
			+ " deleted = :deleted "
			+ "where id = :id")
	public abstract void update(@BindBean final Workflow workflow);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow where id = :id")
	public abstract int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from transaction where id_workflow=:id), "
			+ "	(select count(id) from workflow_step where id_type=2 and id_workflow_external=:id) "
			+ ")) t")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(final int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
