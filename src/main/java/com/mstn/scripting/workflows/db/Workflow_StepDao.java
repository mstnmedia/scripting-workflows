/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla hija {@code workflow_step}.
 *
 * @see WorkflowDao
 * @author josesuero
 */
@RegisterMapper(Workflow_StepDao.Workflow_StepMapper.class)
@UseStringTemplate3StatementLocator
public abstract class Workflow_StepDao {

	/**
	 *
	 */
	static public class Workflow_StepMapper implements ResultSetMapper<Workflow_Step> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_VERSION = "id_workflow_version";
		private static final String GUID = "guid";
		private static final String NAME = "name";
		private static final String ID_TYPE = "id_type";
		private static final String ORDER_INDEX = "order_index";
		private static final String ID_WORKFLOW_EXTERNAL = "id_workflow_external";
		private static final String ID_INTERFACE_SERVICE = "id_interface_service";
		private static final String INITIAL_STEP = "initial_step";

		private static final String WORKFLOW_NAME = "workflow_name";
		private static final String WORKFLOW_MAIN = "workflow_main";
		private static final String WORKFLOW_DELETE = "workflow_deleted";
		private static final String VERSION_ID_VERSION = "version_id_version";
		private static final String VERSION_ACTIVE = "version_active";
		private static final String INTERFACE_SERVICE_NAME = "interface_service_name";

		@Override
		public Workflow_Step map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow_Step(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_VERSION) ? resultSet.getInt(ID_WORKFLOW_VERSION) : 0,
					hasColumn.containsKey(GUID) ? resultSet.getString(GUID) : "",
					hasColumn.containsKey(ID_TYPE) ? resultSet.getInt(ID_TYPE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0,
					hasColumn.containsKey(ID_WORKFLOW_EXTERNAL) ? resultSet.getInt(ID_WORKFLOW_EXTERNAL) : 0,
					hasColumn.containsKey(ID_INTERFACE_SERVICE) ? resultSet.getInt(ID_INTERFACE_SERVICE) : 0,
					hasColumn.containsKey(INITIAL_STEP) ? resultSet.getBoolean(INITIAL_STEP) : false,
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : "",
					hasColumn.containsKey(WORKFLOW_MAIN) ? resultSet.getBoolean(WORKFLOW_MAIN) : false,
					hasColumn.containsKey(WORKFLOW_DELETE) ? resultSet.getBoolean(WORKFLOW_DELETE) : false,
					hasColumn.containsKey(VERSION_ID_VERSION) ? resultSet.getString(VERSION_ID_VERSION) : "",
					hasColumn.containsKey(VERSION_ACTIVE) ? resultSet.getInt(VERSION_ACTIVE) : 0,
					hasColumn.containsKey(INTERFACE_SERVICE_NAME) ? resultSet.getString(INTERFACE_SERVICE_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_workflow_step <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow_Step> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, ws.* from ("
			+ "  select ws.* from v_workflow_step ws <where> order by <order_by>"
			+ " ) ws"
			+ ") ws where ws.rowno >= ((:page_number - 1) * :page_size + 1) and ws.rowno \\<= (:page_number * :page_size)")
	abstract public List<Workflow_Step> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step ws <where> order by order_index, id")
	abstract public List<Workflow_Step> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_version
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step "
			+ "where id_workflow_version = :id_version "
			+ "order by initial_step desc, order_index, id")
	abstract public List<Workflow_Step> getAll(@Bind("id_version") int id_version);

	/**
	 *
	 * @param id_type
	 * @param value
	 * @return
	 */
	@SqlQuery("select ws.* from v_workflow_step ws "
			+ " join workflow_version wv on wv.id = ws.id_workflow_version "
			+ "where ws.id_type=:id_type and ws.id_workflow_external=:value and wv.ACTIVE=0 "
			+ "order by ws.ID_WORKFLOW, ws.ID_WORKFLOW_VERSION, ws.INITIAL_STEP desc, ws.ORDER_INDEX, ws.ID")
	abstract public List<Workflow_Step> getAllByTypeAndValue(@Bind("id_type") int id_type, @Bind("value") int value);

	/**
	 *
	 * @param id_interface_service
	 * @return
	 */
	@SqlQuery("select ws.* from v_workflow_step ws "
			+ " join workflow_version wv on wv.id = ws.id_workflow_version "
			+ "where ws.id_type=3 and ws.id_interface_service=:id_interface_service and wv.ACTIVE=0 "
			+ "order by ws.ID_WORKFLOW, ws.ID_WORKFLOW_VERSION, ws.INITIAL_STEP desc, ws.ORDER_INDEX, ws.ID")
	abstract public List<Workflow_Step> getAllByInterfaceService(@Bind("id_interface_service") int id_interface_service);

	/**
	 *
	 * @param id_step
	 * @return
	 */
	@SqlQuery("with current_step as ("
			+ " select * from workflow_step s where s.id=:id_step"
			+ ")"
			+ "select * "
			+ "from v_workflow_step s "
			+ "where s.id_workflow_version = (select id_workflow_version from current_step)"
			+ " and (select count(o.id) from workflow_step_option o "
			+ "  where o.id_workflow_step=s.id and o.value=to_char(:id_step)) > 0 "
			+ "order by initial_step desc, order_index, id")
	abstract public List<Workflow_Step> getDependentSteps(@Bind("id_step") int id_step);

	/**
	 *
	 * @param id_version
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step ws where ws.id_workflow_version = :id_version and initial_step = 1")
	abstract public Workflow_Step getFirst(@Bind("id_version") int id_version);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step where id = :id")
	abstract public Workflow_Step get(@Bind("id") final int id);

	/**
	 *
	 * @param guid
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step where guid = :guid")
	abstract public Workflow_Step get(@Bind("guid") final String guid);

	/**
	 *
	 * @param id_step
	 * @param value
	 * @return
	 */
	@SqlQuery("select count(ws.id) as Field from v_workflow_step ws "
			+ "join workflow_step_option wso on wso.id_workflow_step = ws.ID "
			+ "where ws.id=:id_step and wso.id_type=" + WorkflowStepOptionTypes.VALUE + " and wso.value=:value")
	abstract public int countOptionsWithValueInStep(@Bind("id_step") final int id_step, @Bind("value") final String value);

	/**
	 *
	 * @param workflow_Step
	 * @return
	 */
	@SqlUpdate("insert into workflow_step(id, id_center, id_workflow, id_workflow_version, guid, name, id_type, order_index, id_workflow_external, id_interface_service, initial_step) "
			+ "values(workflow_step_seq.nextVal, :id_center, :id_workflow, :id_workflow_version, :guid, :name, :id_type, :order_index, :id_workflow_external, :id_interface_service, :initial_step)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Workflow_Step workflow_Step);

	/**
	 *
	 * @param workflow_Step
	 */
	@SqlUpdate("update workflow_step set "
			+ " id_center = :id_center, "
			+ " id_workflow = :id_workflow, "
			+ " id_workflow_version = :id_workflow_version, "
			+ " name = :name, "
			+ " id_type = :id_type, "
			+ " order_index = :order_index, "
			+ " id_workflow_external = :id_workflow_external, "
			+ " id_interface_service = :id_interface_service, "
			+ " initial_step = :initial_step "
			+ "where id = :id")
	abstract public void update(@BindBean final Workflow_Step workflow_Step);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_step where id = :id")
	abstract public int delete(@Bind("id") final int id);
}
