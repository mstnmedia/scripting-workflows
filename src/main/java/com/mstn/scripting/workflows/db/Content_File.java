package com.mstn.scripting.workflows.db;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 * @deprecated
 */
public class Content_File {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected String path;

	/**
	 *
	 */
	protected String tags;

	/**
	 *
	 */
	public Content_File() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param name
	 * @param path
	 * @param tags
	 */
	public Content_File(int id, int id_center, String name, String path, String tags) {
		this.id = id;
		this.id_center = id_center;
		this.name = name;
		this.path = path;
		this.tags = tags;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 *
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 *
	 * @return
	 */
	public String getTags() {
		return tags;
	}

	/**
	 *
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

}
