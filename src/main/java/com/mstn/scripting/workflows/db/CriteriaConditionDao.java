/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Criteria_Condition;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla hija {@code criteria_condition}.
 *
 * @see CriteriaDao
 * @author josesuero
 */
@RegisterMapper(CriteriaConditionDao.CriteriaConditionMapper.class)
@UseStringTemplate3StatementLocator
public interface CriteriaConditionDao {

	/**
	 *
	 */
	public class CriteriaConditionMapper implements ResultSetMapper<Criteria_Condition> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_CRITERIA = "id_criteria";
		private static final String NAME = "name";
		private static final String OPERATOR = "operator";
		private static final String VALUE = "value";

		@Override
		public Criteria_Condition map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Criteria_Condition(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_CRITERIA) ? resultSet.getInt(ID_CRITERIA) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(OPERATOR) ? resultSet.getString(OPERATOR) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from criteria_condition <where> order by ID desc")
	List<Criteria_Condition> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_criteria
	 * @return
	 */
	@SqlQuery("select * from criteria_condition where id_criteria = :id_criteria")
	List<Criteria_Condition> getAll(@Bind("id_criteria") int id_criteria);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from criteria_condition where id = :id")
	Criteria_Condition get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into criteria_condition(id, id_center, id_criteria, name, operator, value)"
			+ " values(criteria_condition_seq.nextVal, :id_center, :id_criteria, :name, :operator, :value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Criteria_Condition item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update criteria_condition set"
			+ " id_center = :id_center,"
			+ " id_criteria = :id_criteria,"
			+ " name = :name,"
			+ " operator = :operator,"
			+ " value = :value "
			+ "where id = :id")
	void update(@BindBean final Criteria_Condition item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from criteria_condition where id = :id")
	int delete(@Bind("id") final int id);
}
