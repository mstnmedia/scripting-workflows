/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.models.Content_Language;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla relacional {@code content_language}.
 *
 * @author josesuero
 * @deprecated
 */
@RegisterMapper(Content_LanguageDao.Content_LanguageMapper.class)
@UseStringTemplate3StatementLocator
public interface Content_LanguageDao {

	/**
	 *
	 */
	public class Content_LanguageMapper implements ResultSetMapper<Content_Language> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_CONTENT = "id_content";
		private static final String ID_LANGUAGE = "id_language";
		private static final String VALUE = "value";

		@Override
		public Content_Language map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			Content_Language item = new Content_Language();
			item.setId(resultSet.getInt(ID));
			item.setId_center(resultSet.getInt(ID_CENTER));
			item.setId_content(resultSet.getInt(ID_CONTENT));
			item.setId_language(resultSet.getInt(ID_LANGUAGE));
			item.setValue(resultSet.getString(VALUE));
			return item;
		}

	}

	/**
	 *
	 * @param id_content
	 * @return
	 */
	@SqlQuery("select * from content_language where id_content = :id_content")
	List<Content_Language> getAll(@Bind("id_content") int id_content);

	@SqlQuery("select * from content_language <where> order by id desc")
	abstract public List<Content_Language> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from content_language where id = :id")
	Content_Language get(@Bind("id") final int id);

	/**
	 *
	 * @param content_language
	 * @return
	 */
	@SqlUpdate("insert into content_language(id, id_center, id_content, id_language, value) values(content_language_seq.nextVal, :id_center, :id_content, :id_language,:value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Content_Language content_language);

	/**
	 * @param content_language
	 */
	@SqlUpdate("update content_language set id_center = coalesce(:id_center, id_center), id_content = coalesce(:id_content, id_content), id_language = coalesce(:id_language, id_language), value = coalesce(:value, value) where id = :id")
	void update(@BindBean final Content_Language content_language);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from content_language where id = :id")
	int delete(@Bind("id") final int id);
}
