/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Workflow_Step;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla relacional {@code workflow_step_content}.
 *
 * @see ContentDao
 * @see Workflow_StepDao
 * @author josesuero
 */
@RegisterMapper(Workflow_Step_ContentDao.Workflow_Step_ContentMapper.class)
@UseStringTemplate3StatementLocator
public abstract class Workflow_Step_ContentDao {

	/**
	 *
	 */
	static public class Workflow_Step_ContentMapper implements ResultSetMapper<Workflow_Step_Content> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_VERSION = "id_workflow_version";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String ID_CONTENT = "id_content";
		private static final String ORDER_INDEX = "order_index";

		private static final String WORKFLOW_STEP_NAME = "workflow_step_name";
		private static final String WORKFLOW_STEP_ORDER = "workflow_step_order";
		private static final String WORKFLOW_NAME = "workflow_name";
		private static final String WORKFLOW_MAIN = "workflow_main";
		private static final String WORKFLOW_DELETED = "workflow_deleted";
		private static final String VERSION_ID_VERSION = "version_id_version";
		private static final String VERSION_ACTIVE = "version_active";

		@Override
		public Workflow_Step_Content map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow_Step_Content(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_VERSION) ? resultSet.getInt(ID_WORKFLOW_VERSION) : 0,
					hasColumn.containsKey(ID_WORKFLOW_STEP) ? resultSet.getInt(ID_WORKFLOW_STEP) : 0,
					hasColumn.containsKey(ID_CONTENT) ? resultSet.getInt(ID_CONTENT) : 0,
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0,
					hasColumn.containsKey(WORKFLOW_STEP_NAME) ? resultSet.getString(WORKFLOW_STEP_NAME) : "",
					hasColumn.containsKey(WORKFLOW_STEP_ORDER) ? resultSet.getInt(WORKFLOW_STEP_ORDER) : 0,
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : "",
					hasColumn.containsKey(WORKFLOW_MAIN) ? resultSet.getInt(WORKFLOW_MAIN) : 0,
					hasColumn.containsKey(WORKFLOW_DELETED) ? resultSet.getInt(WORKFLOW_DELETED) : 0,
					hasColumn.containsKey(VERSION_ID_VERSION) ? resultSet.getString(VERSION_ID_VERSION) : "",
					hasColumn.containsKey(VERSION_ACTIVE) ? resultSet.getInt(VERSION_ACTIVE) : 0
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_workflow_step_content wsc <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Workflow_Step_Content> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, wsc.* from ("
			+ "  select wsc.* from v_workflow_step_content wsc <where> order by <order_by>"
			+ " ) wsc"
			+ ") wsc where wsc.rowno >= ((:page_number - 1) * :page_size + 1) and wsc.rowno \\<= (:page_number * :page_size)")
	abstract public List<Workflow_Step_Content> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step_content wsc <where> order by id desc")
	abstract public List<Workflow_Step_Content> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_step
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step_content where id_workflow_step = :id_step order by order_index, id")
	abstract public List<Workflow_Step_Content> getAll(@Bind("id_step") final int id_step);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_workflow_step_content where id = :id")
	abstract public Workflow_Step_Content get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @param step
	 * @return
	 */
	public int insert(Workflow_Step_Content item, Workflow_Step step) {
		item.setId_workflow_step(step.getId());
		item.setId_workflow_version(step.getId_workflow_version());
		return insert(item);
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into workflow_step_content(id, id_center, id_workflow, id_workflow_version, id_workflow_step, id_content, order_index) values(workflow_step_content_seq.nextVal, :id_center, :id_workflow, :id_workflow_version, :id_workflow_step, :id_content, :order_index)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Workflow_Step_Content item);

	/**
	 *
	 * @param workflow_Step
	 */
	@SqlUpdate("update workflow_step_content set "
			+ " id_center = :id_center,"
			+ " id_workflow = :id_workflow,"
			+ " id_workflow_version = :id_workflow_version,"
			+ " id_workflow_step = :id_workflow_step,"
			+ " id_content = :id_content,"
			+ " order_index = :order_index "
			+ "where id = :id")
	abstract public void update(@BindBean final Workflow_Step_Content workflow_Step);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_step_content where id = :id")
	abstract public int delete(@Bind("id") final int id);
}
