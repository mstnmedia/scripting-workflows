/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.models.Content;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.ClobValue;
import com.mstn.scripting.core.db.ClobValueBinder;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code content}.
 *
 * @author MSTN Media
 */
@RegisterMapper(ContentDao.ContentMapper.class)
@UseStringTemplate3StatementLocator
public abstract class ContentDao {

	/**
	 *
	 */
	static public class ContentMapper implements ResultSetMapper<Content> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String CENTER_NAME = "center_name";
		private static final String NAME = "name";
		private static final String TAGS = "tags";
		private static final String TYPE = "id_type";
		private static final String VALUE = "value";
		private static final String FILENAME = "filename";
		private static final String DELETED = "deleted";

		@Override
		public Content map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Content(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(TAGS) ? resultSet.getString(TAGS) : "",
					hasColumn.containsKey(TYPE) ? resultSet.getInt(TYPE) : 0,
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "",
					hasColumn.containsKey(FILENAME) ? resultSet.getString(FILENAME) : "",
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : false
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_content <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Content> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, c.* from ("
			+ "  select c.* from v_content c <where> order by <order_by>"
			+ " ) c"
			+ ") c where c.rowno >= ((:page_number - 1) * :page_size + 1) and c.rowno \\<= (:page_number * :page_size)")
	abstract public List<Content> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_content <where> order by id desc")
	abstract public List<Content> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_content where id = :id")
	abstract public Content get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 * @return
	 */
	@SqlUpdate("insert into content(id, id_center, name, tags, id_type, value, filename, deleted)"
			+ " values(content_seq.nextVal, :id_center, :name, :tags, :id_type, <value>, :filename, :deleted)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Content item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 * @return
	 */
	public int insert(@BindBean final Content item) {
		ClobValue value = new ClobValue(item.getValue());
		return insert(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update content set "
			+ "	id_center = :id_center, "
			+ "	name = :name,"
			+ " tags = :tags, "
			+ "	id_type = :id_type, "
			+ "	value = <value>, "
			+ "	filename = :filename, "
			+ "	deleted = :deleted "
			+ "where id = :id")
	abstract public void update(@BindBean final Content item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 */
	public void update(Content item) {
		ClobValue value = new ClobValue(item.getValue());
		update(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from content where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from workflow_step_content where id_content=:id) "
			+ ")) t")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(final int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
