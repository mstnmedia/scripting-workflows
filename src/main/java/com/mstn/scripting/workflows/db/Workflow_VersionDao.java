/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code workflow_version}.
 *
 * @see WorkflowDao
 * @author josesuero
 */
@RegisterMapper(Workflow_VersionDao.Workflow_VersionMapper.class)
@UseStringTemplate3StatementLocator
public interface Workflow_VersionDao {

	/**
	 *
	 */
	public class Workflow_VersionMapper implements ResultSetMapper<Workflow_Version> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_VERSION = "id_version";
		private static final String DOCDATE = "docdate";
		private static final String ACTIVE = "active";
		private static final String NOTES = "notes";
		private static final String STRUCTURE = "structure";

		private static final String WORKFLOW_NAME = "workflow_name";

		@Override
		public Workflow_Version map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow_Version(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_VERSION) ? resultSet.getString(ID_VERSION) : "",
					hasColumn.containsKey(DOCDATE) ? resultSet.getDate(DOCDATE) : null,
					hasColumn.containsKey(ACTIVE) ? resultSet.getInt(ACTIVE) : 0,
					hasColumn.containsKey(NOTES) ? resultSet.getString(NOTES) : "",
					hasColumn.containsKey(STRUCTURE) ? resultSet.getString(STRUCTURE) : "",
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_workflow_version <where> order by id desc")
	List<Workflow_Version> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_workflow
	 * @return
	 */
	@SqlQuery("select * from v_workflow_version where id_workflow = :id_workflow")
	List<Workflow_Version> getAll(@Bind("id_workflow") final int id_workflow);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_workflow_version where id = :id")
	Workflow_Version get(@Bind("id") final int id);

	/**
	 *
	 * @param workflow_version
	 * @return
	 */
	@SqlUpdate("insert into workflow_version(id, id_center, id_workflow, id_version, docdate, active, notes, structure)"
			+ " values(workflow_version_seq.nextVal, :id_center, :id_workflow, :id_version, :docdate, :active, :notes, :structure)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Workflow_Version workflow_version);

	/**
	 *
	 * @param workflow_version
	 */
	@SqlUpdate("update workflow_version set "
			+ " id_center = :id_center, "
			+ " id_workflow = :id_workflow, "
			+ " id_version = :id_version, "
			//+ " docdate = :docdate, "
			//+ " structure = :structure "
			+ " notes = :notes, "
			+ " active = :active "
			+ "where id = :id")
	void update(@BindBean final Workflow_Version workflow_version);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_version where id = :id")
	int delete(@Bind("id") final int id);
}
