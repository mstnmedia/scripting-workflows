/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.models.Workflow_Criteria;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla relacional {@code workflow_criteria}.
 *
 * @see WorkflowDao
 * @see CriteriaDao
 * @author josesuero
 */
@RegisterMapper(Workflow_CriteriaDao.Workflow_CriteriaMapper.class)
@UseStringTemplate3StatementLocator
public interface Workflow_CriteriaDao {

	/**
	 *
	 */
	static public class Workflow_CriteriaMapper implements ResultSetMapper<Workflow_Criteria> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_CRITERIA = "id_criteria";
		private static final String NAME = "name";
		private static final String VALUE = "value";
		private static final String WORKFLOW_NAME = "workflow_name";
		private static final String CRITERIA_NAME = "criteria_name";

		@Override
		public Workflow_Criteria map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow_Criteria(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_CRITERIA) ? resultSet.getInt(ID_CRITERIA) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "",
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : "",
					hasColumn.containsKey(CRITERIA_NAME) ? resultSet.getString(CRITERIA_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_workflow_criteria <where> order by id")
	List<Workflow_Criteria> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_workflow
	 * @return
	 */
	@SqlQuery("select * from v_workflow_criteria where id_workflow = :id_workflow order by id")
	List<Workflow_Criteria> getAll(@Bind("id_workflow") final int id_workflow);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_workflow_criteria where id = :id")
	Workflow_Criteria get(@Bind("id") final int id);

	/**
	 *
	 * @param workflow_Criteria
	 * @return
	 */
	@SqlUpdate("insert into workflow_criteria(id, id_center, id_workflow, id_criteria, name, value) values(workflow_criteria_seq.nextVal, :id_center, :id_workflow, :id_criteria, :name,:value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Workflow_Criteria workflow_Criteria);

	/**
	 *
	 * @param workflow_Criteria
	 */
	@SqlUpdate("update workflow_criteria set "
			+ " id_center = :id_center,"
			+ " id_workflow = :id_workflow,"
			+ " name = :name,"
			+ " value = :value "
			+ "where id = :id")
	void update(@BindBean final Workflow_Criteria workflow_Criteria);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_criteria where id = :id")
	int delete(@Bind("id") final int id);
}
