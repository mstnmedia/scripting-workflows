/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.models.Workflow_Step_Interface_Field;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla hija {@code workflow_step_interface_field}.
 *
 * @see Workflow_StepDao
 * @author josesuero
 */
@RegisterMapper(Workflow_Step_Interface_FieldDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface Workflow_Step_Interface_FieldDao {

	/**
	 *
	 */
	public class Mapper implements ResultSetMapper<Workflow_Step_Interface_Field> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String ID_INTERFACE = "id_interface";
		private static final String ID_INTERFACE_FIELD = "id_interface_field";
		private static final String ID_INTERFACE_SOURCE = "id_interface_source";
		private static final String ID_INTERFACE_FIELD_SOURCE = "id_interface_field_source";

		@Override
		public Workflow_Step_Interface_Field map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			return new Workflow_Step_Interface_Field(
					resultSet.getInt(ID),
					resultSet.getInt(ID_CENTER),
					resultSet.getInt(ID_WORKFLOW_STEP),
					resultSet.getInt(ID_INTERFACE),
					resultSet.getString(ID_INTERFACE_FIELD),
					resultSet.getString(ID_INTERFACE_SOURCE),
					resultSet.getString(ID_INTERFACE_FIELD_SOURCE)
			);
		}
	}

	/**
	 *
	 * @param id_step
	 * @return
	 */
	@SqlQuery("select * from workflow_step_interface_field where id_workflow_step = :id_step")
	List<Workflow_Step_Interface_Field> getAll(@Bind("id_step") final int id_step);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from workflow_step_interface_field <where>")
	List<Workflow_Step_Interface_Field> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from workflow_step_interface_field where id = :id")
	Workflow_Step_Interface_Field get(@Bind("id") final int id);

	/**
	 *
	 * @param workflow_Step_Interface_Field
	 * @return
	 */
	@SqlUpdate("insert into workflow_step_interface_field(id, id_center, id_workflow_step, id_interface, id_interface_field, id_interface_source, id_interface_field_source) "
			+ "values(wf_step_if_field_seq.nextVal, :id_center, :id_workflow_step, :id_interface, :id_interface_field, :id_interface_source, :id_interface_field_source)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Workflow_Step_Interface_Field workflow_Step_Interface_Field);

	/**
	 *
	 * @param workflow_Step_Interface_Field
	 */
	@SqlUpdate("update workflow_step_interface_field set "
			+ " id_center = :id_center, "
			+ " id_workflow_step = :id_workflow_step, "
			+ " id_interface = :id_interface, "
			+ " id_interface_field = :id_interface_field, "
			+ " id_interface_source = :id_interface_source, "
			+ " id_interface_field_source = :id_interface_field_source "
			+ "where id = :id")
	void update(@BindBean final Workflow_Step_Interface_Field workflow_Step_Interface_Field);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_step_interface_field where id = :id")
	int delete(@Bind("id") final int id);
}
