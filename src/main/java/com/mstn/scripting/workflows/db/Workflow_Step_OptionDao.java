/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.mstn.scripting.core.models.Workflow_Step_Option;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla hija {@code workflow_step_option}.
 *
 * @see Workflow_Step_Dao
 * @author josesuero
 */
@RegisterMapper(Workflow_Step_OptionDao.Workflow_Step_OptionMapper.class)
@UseStringTemplate3StatementLocator
public interface Workflow_Step_OptionDao {

	/**
	 *
	 */
	public class Workflow_Step_OptionMapper implements ResultSetMapper<Workflow_Step_Option> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_VERSION = "id_workflow_version";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String GUID_STEP = "guid_step";
		private static final String ID_TYPE = "id_type";
		private static final String NAME = "name";
		private static final String COLOR = "color";
		private static final String VALUE = "value";
		private static final String ORDER_INDEX = "order_index";

		@Override
		public Workflow_Step_Option map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Workflow_Step_Option(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_VERSION) ? resultSet.getInt(ID_WORKFLOW_VERSION) : 0,
					hasColumn.containsKey(ID_WORKFLOW_STEP) ? resultSet.getInt(ID_WORKFLOW_STEP) : 0,
					hasColumn.containsKey(GUID_STEP) ? resultSet.getString(GUID_STEP) : "",
					hasColumn.containsKey(ID_TYPE) ? resultSet.getInt(ID_TYPE) : WorkflowStepOptionTypes.UNDEFINED,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(COLOR) ? resultSet.getString(COLOR) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "",
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0
			);
		}
	}

	/**
	 *
	 * @param id_step
	 * @return
	 */
	@SqlQuery("select * from workflow_step_option where id_workflow_step = :id_step order by order_index, id")
	List<Workflow_Step_Option> getAll(@Bind("id_step") final int id_step);

//	@SqlQuery("select * from workflow_step_option where guid_step = :guid_step order by order_index, id")
//	List<Workflow_Step_Option> getAll(@Bind("guid_step") final String guid_step);
	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from workflow_step_option <where> order by order_index, id")
	List<Workflow_Step_Option> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	@SqlQuery("select wso.* "
			+ "from workflow_step_option wso "
			+ " join workflow_step ws on ws.id=wso.id_workflow_step "
			+ " join workflow_version wv on wv.id=ws.id_workflow_version "
			+ "<where> "
			+ "order by wso.order_index, wso.id")
	List<Workflow_Step_Option> getAllJoin(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from workflow_step_option where id = :id")
	Workflow_Step_Option get(@Bind("id") final int id);

	/**
	 *
	 * @param id_option
	 * @return
	 */
	@SqlQuery("select id_workflow from workflow_step_option wso where wso.id = :id_option")
	int getIdWorkflowFromIdOption(@Bind("id") final int id_option);

	/**
	 *
	 * @param workflow_Step_Option
	 * @return
	 */
	@SqlUpdate("insert into workflow_step_option(id, id_center, id_workflow, id_workflow_version, id_workflow_step, guid_step, id_type, name, color, value, order_index) "
			+ "values(workflow_step_option_seq.nextVal, :id_center, :id_workflow, :id_workflow_version, :id_workflow_step, :guid_step, :id_type, :name, :color, (case "
			+ "when :id_type=1 then to_char((select id from workflow_step ws where ws.guid = :guid_step and ws.id_workflow_version = :id_workflow_version)) "
			+ "when :id_type=2 then to_char(:value) else '' end), :order_index)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Workflow_Step_Option workflow_Step_Option);

	/**
	 *
	 * @param workflow_Step_Option
	 */
	@SqlUpdate("update workflow_step_option set "
			+ "id_center = :id_center, "
			+ "id_workflow = :id_workflow, "
			+ "id_workflow_version = :id_workflow_version, "
			+ "id_workflow_step = :id_workflow_step, "
			+ "guid_step = :guid_step, "
			+ "id_type = :id_type, "
			+ "name = :name, "
			+ "color = :color, "
			+ "value = :value, "
			+ "order_index = :order_index "
			+ "where id = :id")
	void update(@BindBean final Workflow_Step_Option workflow_Step_Option);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from workflow_step_option where id = :id")
	int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id_workflow_step
	 * @return
	 */
	@SqlUpdate("delete from workflow_step_option where id_workflow_step=:id_workflow_step")
	int deleteFromStep(@Bind("id_workflow_step") int id_workflow_step);
}
