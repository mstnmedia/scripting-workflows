package com.mstn.scripting.workflows.db;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que definiría los tipos de flujos permitidos.
 *
 * @author amatos
 * @deprecated
 */
public class Workflow_Type {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	public Workflow_Type() {

	}

	/**
	 *
	 * @param id
	 * @param name
	 */
	public Workflow_Type(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
