/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.db;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.Date;

/**
 * Clase que define las propiedades de una versión de flujo.
 *
 * @author josesuero
 */
public class Workflow_Version {

	private int id;

	/**
	 *
	 */
	protected int id_center;
	private int id_workflow;
	private String id_version;
	@JsonDeserialize(using = JSON.DateDeserializer.class)
	private Date docdate;
	private int active;
	private String notes;
	private String structure;
	private String workflow_name;

	/**
	 * Constructor
	 */
	public Workflow_Version() {

	}

	/**
	 * Constructor con parámetros
	 *
	 * @param id ID de la versión de flujo
	 * @param id_center ID del centro del flujo
	 * @param id_workflow ID del flujo
	 * @param id_version Nombre de la versión
	 * @param docdate Fecha de publicación
	 * @param active Estado de la versión (0: editable; 1: publicada; 2:
	 * archivada)
	 * @param notes Notas de la publicación
	 * @param structure Definición del flujo en formato JSON (está en desuso).
	 * Usar texto vacío.
	 * @param workflow_name Nombre del flujo
	 */
	public Workflow_Version(
			int id, int id_center, int id_workflow,
			String id_version, Date docdate,
			int active, String notes, String structure,
			String workflow_name
	) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_version = id_version;
		this.docdate = docdate;
		this.active = active;
		this.notes = notes;
		this.structure = structure;
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public String getId_version() {
		return id_version;
	}

	/**
	 *
	 * @param id_version
	 */
	public void setId_version(String id_version) {
		this.id_version = id_version;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getDocdate() {
		return docdate;
	}

	/**
	 *
	 * @param docdate
	 */
	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	/**
	 *
	 * @return
	 */
	public int getActive() {
		return active;
	}

	/**
	 *
	 * @param active
	 */
	public void setActive(int active) {
		this.active = active;
	}

	/**
	 *
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 *
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 *
	 * @return
	 */
	public String getStructure() {
		return structure;
	}

	/**
	 *
	 * @param structure
	 */
	public void setStructure(String structure) {
		this.structure = structure;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

}
