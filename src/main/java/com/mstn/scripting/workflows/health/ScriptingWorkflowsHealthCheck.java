/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.workflows.health;

import com.codahale.metrics.health.HealthCheck;
import com.mstn.scripting.workflows.core.WorkflowService;

/**
 * Clase que se registra para consultar el estado del microservicio.
 *
 * @author josesuero
 */
public class ScriptingWorkflowsHealthCheck extends HealthCheck {

    private static final String HEALTHY = "The Workflow Service is healthy for read and write";
    private static final String UNHEALTHY = "The Workflow Service is not healthy. ";
    private static final String MESSAGE_PLACEHOLDER = "{}";

    private final WorkflowService workflowService;

	/**
	 *
	 * @param workflowService
	 */
	public ScriptingWorkflowsHealthCheck(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Override
    protected Result check() throws Exception {
        String dbHealthStatus = workflowService.performHealthCheck();

        if (dbHealthStatus == null) {
            return Result.healthy(HEALTHY);
        } else {
            return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, dbHealthStatus);
        }

    }

}
